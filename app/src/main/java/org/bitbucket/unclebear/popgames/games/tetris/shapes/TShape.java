package org.bitbucket.unclebear.popgames.games.tetris.shapes;

import org.bitbucket.unclebear.popgames.games.tetris.Shape;

public class TShape extends Shape {
    public TShape(int x, int y) {
        super(x, y);
        boolean[][] block = new boolean[][]{{false, true}, {true, true}, {false, true}};
        setBlock(block);
    }
}

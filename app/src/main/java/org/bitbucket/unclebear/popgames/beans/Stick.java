package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import org.bitbucket.unclebear.popgames.utils.Dimension;

public class Stick {
    private Rect background;
    private Paint backgroundPaint;

    public Stick(boolean isHorizontal, int x, int y, int size) {
        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.BLACK);
        backgroundPaint.setAlpha(Dimension.LCD_OFF_ALPHA);
        backgroundPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        final float widthFactor = 0.2f;
        int width = (int) (size * widthFactor);
        width = width < 1 ? 1 : width;
        if (isHorizontal) {
            background = new Rect(x + width, y + width, x + size, y);
        } else {
            background = new Rect(x + width, y + width, x, y + size);
        }
    }

    public void draw(Canvas canvas) {
        canvas.drawRect(background, backgroundPaint);
    }

    public void lightUp(boolean willLightUp) {
        backgroundPaint.setAlpha(willLightUp ? Dimension.LCD_ON_ALPHA : Dimension.LCD_OFF_ALPHA);
    }
}

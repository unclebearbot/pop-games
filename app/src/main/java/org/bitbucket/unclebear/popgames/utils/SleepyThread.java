package org.bitbucket.unclebear.popgames.utils;

import android.util.Log;

public class SleepyThread extends Thread {
    private boolean isRunning;
    private long interval;
    private Runnable runnable;

    public SleepyThread(long interval, Runnable runnable) {
        this.interval = interval;
        this.runnable = runnable;
        isRunning = true;
    }

    @Override
    public void run() {
        while (isRunning) {
            long beginningTimestamp = System.nanoTime();
            runnable.run();
            long endingTimestamp = System.nanoTime();
            long elapsedTime = endingTimestamp - beginningTimestamp;
            long freeTime = interval - elapsedTime;
            if (freeTime > 0) {
                try {
                    Thread.sleep(freeTime / Dimension.MILLISECOND);
                } catch (InterruptedException e) {
                    Log.e(Dimension.ERROR, e.getMessage(), e);
                    terminate();
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public void terminate() {
        isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }
}

package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import org.bitbucket.unclebear.popgames.utils.Dimension;

public class Border {
    boolean isUp;
    boolean isDown;
    boolean isLeft;
    boolean isRight;
    private boolean isDark;
    private int x;
    private int y;
    private int width;
    private int height;
    private float[][] lines;
    private Paint linesPaint;
    private int alphaOffset;

    public Border(boolean isDark, int x, int y, int width, int height, boolean isLeftOrUp, boolean isRightOrUp) {
        this.isDark = isDark;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        if (isLeftOrUp && isRightOrUp) {
            isUp = true;
        } else if (!isLeftOrUp && !isRightOrUp) {
            isDown = true;
        } else if (isLeftOrUp) {
            isLeft = true;
        } else {
            isRight = true;
        }
    }

    public void draw(Canvas canvas) {
        reset();
        if (isLeft || isUp) {
            for (float[] line : lines) {
                int alpha = linesPaint.getAlpha() - alphaOffset;
                linesPaint.setAlpha(alpha);
                canvas.drawLines(line, linesPaint);
            }
        } else {
            for (int i = lines.length; i > 0; --i) {
                int alpha = linesPaint.getAlpha() - alphaOffset;
                linesPaint.setAlpha(alpha);
                canvas.drawLines(lines[i - 1], linesPaint);
            }
        }
    }

    private void reset() {
        linesPaint = new Paint();
        linesPaint.setColor(Color.BLACK);
        int alpha = isDark ? Dimension.BORDER_DARK_ALPHA : Dimension.BORDER_LIGHT_ALPHA;
        if (isUp || isDown) {
            alphaOffset = alpha / height;
            linesPaint.setAlpha(alphaOffset * height);
            lines = new float[height][4];
            for (int i = 0; i < height; ++i) {
                lines[i][0] = x;
                lines[i][1] = y + (float) i;
                lines[i][2] = lines[i][0] + width;
                lines[i][3] = lines[i][1];
            }
        } else {
            alphaOffset = alpha / width;
            linesPaint.setAlpha(alphaOffset * width);
            lines = new float[width][4];
            for (int i = 0; i < width; ++i) {
                lines[i][0] = x + (float) i;
                lines[i][1] = y;
                lines[i][2] = lines[i][0];
                lines[i][3] = lines[i][1] + height;
            }
        }
    }
}

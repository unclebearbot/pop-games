package org.bitbucket.unclebear.popgames.games.arrow;

import org.bitbucket.unclebear.popgames.games.Role;

class Hero extends Role {
    private boolean[][] normal = new boolean[][]{{false, false}, {false, true}, {true, true}, {false, true}, {false, false}, {false, false}, {false, false}};
    private boolean[][] promoted = new boolean[][]{{false, false}, {false, true}, {true, true}, {false, true}, {true, true}, {false, true}, {false, false}};
    private boolean isPromoted;

    Hero(int x, int y) {
        super(x, y);
        setBlock(normal);
    }

    boolean isPromoted() {
        return isPromoted;
    }

    void setPromoted(boolean promoted) {
        isPromoted = promoted;
        setBlock(isPromoted ? this.promoted : normal);
    }
}

package org.bitbucket.unclebear.popgames.utils;

import android.content.Intent;
import android.net.Uri;

public class Website {
    private Website() {
    }

    public static void view(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(uri);
        Resource.getContext().startActivity(intent);
    }
}

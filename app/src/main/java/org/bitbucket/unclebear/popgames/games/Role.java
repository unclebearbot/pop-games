package org.bitbucket.unclebear.popgames.games;

import org.bitbucket.unclebear.popgames.utils.Dimension;

public abstract class Role {
    private boolean[][] block = new boolean[][]{};
    private int x;
    private int y;

    public Role(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isInBlockAndUp(int x, int y) {
        boolean isInRange = 0 <= x && x < block.length && 0 <= y && y < block[x].length;
        return isInRange && block[x][y];
    }

    public void remove(int x, int y) {
        if (isInBlockAndUp(x, y)) {
            block[x][y] = false;
        }
    }

    public void clear() {
        block = new boolean[Dimension.PIXELS_AMOUNT_X][Dimension.PIXELS_AMOUNT_Y - 2];
    }

    public boolean[][] getBlock() {
        return block;
    }

    public void setBlock(boolean[][] block) {
        this.block = block;
    }
}

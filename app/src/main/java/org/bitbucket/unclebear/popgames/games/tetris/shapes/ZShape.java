package org.bitbucket.unclebear.popgames.games.tetris.shapes;

import org.bitbucket.unclebear.popgames.games.tetris.Shape;

public class ZShape extends Shape {
    public ZShape(int x, int y) {
        super(x, y);
        boolean[][] block = new boolean[][]{{true, false}, {true, true}, {false, true}};
        setBlock(block);
    }
}

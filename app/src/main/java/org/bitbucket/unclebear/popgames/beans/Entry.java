package org.bitbucket.unclebear.popgames.beans;

public class Entry {
    private String key;
    private Number value;
    private String valueName;
    private Number[] values;
    private String[] valueNames;
    private Function function;

    public Entry(String key, String[] valueNames, Number[] values) {
        this.key = key;
        this.values = values;
        this.value = this.values[0];
        this.valueNames = valueNames;
        this.valueName = valueNames[0];
    }

    public void change(boolean isForward) {
        boolean isHit = false;
        for (int i = 0; i < values.length; ++i) {
            if (value.equals(values[i])) {
                isHit = true;
                value = adjustValueIfHit(values, isForward, i);
                valueName = adjustValueIfHit(valueNames, isForward, i);
                break;
            }
        }
        resetValueIfNotHit(isHit);
        performFunction();
    }

    private <T> T adjustValueIfHit(T[] ts, boolean isForward, int index) {
        return isForward ? index == ts.length - 1 ? ts[0] : ts[index + 1] : index == 0 ? ts[ts.length - 1] : ts[index - 1];
    }

    private void resetValueIfNotHit(boolean isHit) {
        if (!isHit) {
            value = values[0];
            valueName = valueNames[0];
        }
    }

    private void performFunction() {
        if (function != null) {
            function.perform();
        }
    }

    public String getKey() {
        return key;
    }

    public Number getValue() {
        return value;
    }

    public String getValueName() {
        return valueName;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    @FunctionalInterface
    public interface Function {
        void perform();
    }
}

package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import org.bitbucket.unclebear.popgames.utils.Dimension;

public class Pixel {
    private Rect outerRect;
    private Rect innerRect;
    private Paint outerPaint;
    private Paint innerPaint;
    private int x;
    private int y;
    private boolean isUp;

    public Pixel(int x, int y, int size) {
        this.x = x;
        this.y = y;
        final float marginFactor = 0.2f;
        int margin = (int) (size * marginFactor);
        margin = margin < 2 ? 2 : margin;
        final float paddingFactor = 0.225f;
        int padding = (int) (size * paddingFactor);
        padding = padding < 2 ? 2 : padding;
        final float strokeFactor = 0.1f;
        int stroke = (int) (size * strokeFactor);
        stroke = stroke < 1 ? 1 : stroke;
        int outerLeft = this.x + margin;
        int outerTop = this.y + margin;
        int outerRight = outerLeft + size - margin;
        int outerBottom = outerTop + size - margin;
        outerRect = new Rect(outerLeft, outerTop, outerRight, outerBottom);
        int innerLeft = outerLeft + padding;
        int innerTop = outerTop + padding;
        int innerRight = outerRight - padding;
        int innerBottom = outerBottom - padding;
        innerRect = new Rect(innerLeft, innerTop, innerRight, innerBottom);
        outerPaint = new Paint();
        outerPaint.setColor(Color.BLACK);
        outerPaint.setAlpha(Dimension.LCD_OFF_ALPHA);
        outerPaint.setStyle(Paint.Style.STROKE);
        outerPaint.setStrokeWidth(stroke);
        innerPaint = new Paint();
        innerPaint.setColor(Color.BLACK);
        innerPaint.setAlpha(Dimension.LCD_OFF_ALPHA);
        innerPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        innerPaint.setStrokeWidth(stroke);
    }

    public void draw(Canvas canvas) {
        canvas.drawRect(outerRect, outerPaint);
        canvas.drawRect(innerRect, innerPaint);
    }

    public void lightUp(boolean willLightUp) {
        isUp = willLightUp;
        outerPaint.setAlpha(isUp ? Dimension.LCD_ON_ALPHA : Dimension.LCD_OFF_ALPHA);
        innerPaint.setAlpha(isUp ? Dimension.LCD_ON_ALPHA : Dimension.LCD_OFF_ALPHA);
    }

    public boolean isUp() {
        return isUp;
    }

    public void reverse() {
        lightUp(!isUp);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}

package org.bitbucket.unclebear.popgames.games.tetris.shapes;

import org.bitbucket.unclebear.popgames.games.tetris.Shape;

public class LShape extends Shape {
    public LShape(int x, int y) {
        super(x, y);
        boolean[][] block = new boolean[][]{{false, true}, {false, true}, {true, true}};
        setBlock(block);
    }
}

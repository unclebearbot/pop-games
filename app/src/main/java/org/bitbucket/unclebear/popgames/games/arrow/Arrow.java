package org.bitbucket.unclebear.popgames.games.arrow;

import org.bitbucket.unclebear.popgames.games.Role;

class Arrow extends Role {
    private boolean isCollided;

    Arrow(int x, int y) {
        super(x, y);
    }

    boolean isCollided() {
        return isCollided;
    }

    void setCollided(boolean collided) {
        isCollided = collided;
    }
}

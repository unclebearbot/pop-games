package org.bitbucket.unclebear.popgames.controllers;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.utils.Resource;
import org.bitbucket.unclebear.popgames.utils.Setting;

import java.util.HashMap;

public class Sound {
    public static final float MAX_VOLUME = 1f;
    private static SoundPool soundPool;
    private static HashMap<Integer, MediaPlayer> musics;
    private static HashMap<Integer, Integer> effects;
    private static float musicVolume = MAX_VOLUME;
    private static float effectVolume = MAX_VOLUME;
    private static int playingMusicId;

    static {
        int[] effectIds = new int[]{R.raw.effect_click, R.raw.effect_negative, R.raw.effect_positive, R.raw.effect_alert, R.raw.effect_reward};
        effects = new HashMap<>();
        final int maxStreamCount = 2;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            soundPool = new SoundPool.Builder().setMaxStreams(maxStreamCount).setAudioAttributes(new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build()).build();
        } else {
            //noinspection deprecation
            soundPool = new SoundPool(maxStreamCount, AudioManager.STREAM_MUSIC, 0);
        }
        for (int id : effectIds) {
            effects.put(id, soundPool.load(Resource.getContext(), id, 1));
        }
        int[] musicIds = new int[]{R.raw.music_welcome, R.raw.music_game_beginning, R.raw.music_game_over};
        musics = new HashMap<>();
        for (int id : musicIds) {
            MediaPlayer mediaPlayer = MediaPlayer.create(Resource.getContext(), id);
            mediaPlayer.setOnCompletionListener(MediaPlayer::pause);
            musics.put(id, mediaPlayer);
        }
        musicVolume = Setting.get(Setting.MUSIC, MAX_VOLUME);
        effectVolume = Setting.get(Setting.EFFECT, MAX_VOLUME);
    }

    private Sound() {
    }

    public static void setMusicVolume(float musicVolume) {
        Sound.musicVolume = musicVolume;
        Setting.set(Setting.MUSIC, musicVolume);
        if (musics.containsKey(playingMusicId)) {
            musics.get(playingMusicId).setVolume(musicVolume, musicVolume);
        }
    }

    public static void setEffectVolume(float effectVolume) {
        Sound.effectVolume = effectVolume;
        Setting.set(Setting.EFFECT, effectVolume);
    }

    public static void play(int id) {
        if (effects == null || musics == null) {
            return;
        }
        if (effects.containsKey(id)) {
            playEffect(id);
        } else if (musics.containsKey(id)) {
            playMusic(id);
        }
    }

    private static void playEffect(int id) {
        if (effectVolume < 0.01f) {
            return;
        }
        soundPool.play(effects.get(id), effectVolume, effectVolume, 1, 0, 1);
    }

    private static void playMusic(int id) {
        if (musicVolume < 0.01f) {
            return;
        }
        if (musics.containsKey(playingMusicId)) {
            MediaPlayer lastMediaPlayer = musics.get(playingMusicId);
            if (lastMediaPlayer != null && lastMediaPlayer.isPlaying()) {
                lastMediaPlayer.pause();
            }
        }
        playingMusicId = id;
        if (musics.containsKey(id)) {
            MediaPlayer mediaPlayer = musics.get(id);
            mediaPlayer.setVolume(musicVolume, musicVolume);
            mediaPlayer.seekTo(0);
            mediaPlayer.start();
        }
    }

    public static void terminate() {
        if (musics == null) {
            return;
        }
        if (musics.containsKey(playingMusicId)) {
            MediaPlayer nowMediaPlayer = musics.get(playingMusicId);
            if (nowMediaPlayer != null && nowMediaPlayer.isPlaying()) {
                nowMediaPlayer.pause();
            }
        }
    }
}

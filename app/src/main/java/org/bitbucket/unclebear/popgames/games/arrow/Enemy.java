package org.bitbucket.unclebear.popgames.games.arrow;

import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.games.Role;
import org.bitbucket.unclebear.popgames.utils.Dimension;

class Enemy extends Role {

    Enemy(int x, int y) {
        super(x, y);
        setBlock(new boolean[Dimension.PIXELS_AMOUNT_X][Dimension.PIXELS_AMOUNT_Y - 2]);
    }

    boolean moveDownWithoutCollisions() {
        if (isTouchedBottom()) {
            return false;
        }
        boolean[][] tempBlock = getBlock();
        for (boolean[] line : tempBlock) {
            System.arraycopy(line, 0, line, 1, Dimension.PIXELS_AMOUNT_Y - 3);
        }
        for (boolean[] line : tempBlock) {
            line[0] = Math.random() < Hud.getLevel() / 10f;
        }
        setBlock(tempBlock);
        return true;
    }

    boolean isTouchedBottom() {
        for (boolean[] line : getBlock()) {
            if (line[Dimension.PIXELS_AMOUNT_Y - 3]) {
                return true;
            }
        }
        return false;
    }
}

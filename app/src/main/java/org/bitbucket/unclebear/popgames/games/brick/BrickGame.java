package org.bitbucket.unclebear.popgames.games.brick;

import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

public class BrickGame extends Game {
    private Brick brick;
    private Ball ball;
    private Tablet tablet;

    public BrickGame() {
        setLevelUpScoreBase(0);
        setLevelUpScoreInterval(0);
        tablet = new Tablet(Dimension.PIXELS_AMOUNT_X / 2 - 3, Dimension.PIXELS_AMOUNT_Y - 1);
    }

    private void moveBall() {
        int previousX = ball.getX();
        int previousY = ball.getY();
        ball.moveOneStep(brick, tablet);
        Screen.updatePixels(false, new int[][]{{previousX, previousY}});
        if (ball.isDropped()) {
            ball = new Ball(tablet.getX() + 2, tablet.getY() - 1);
            finallyDecreaseLife();
            return;
        }
        if (brick.isCleared()) {
            increaseLevel();
            terminate();
            start();
        }
    }

    @Override
    public void start() {
        Screen.clearPixels();
        brick = new Brick(this, 0, 0);
        ball = new Ball(tablet.getX() + 2, tablet.getY() - 1);
        brick.fill(Hud.getLevel() + 2);
        resume();
    }
    @Override
    public void resume() {
        clearSleepyThreads();
        int baseSpeed = 4;
        addSleepyThread(new SleepyThread((int) (Dimension.SECOND / (baseSpeed * (1 + Hud.getSpeed() / 5f))), this::moveBall));
        startAllSleepyThreads();
    }

    @Override
    public void pressButton(Button.Key key) {
        switch (key) {
            case LEFT:
                if (tablet.getX() >= 0) {
                    tablet.setX(tablet.getX() - 1);
                }
                break;
            case RIGHT:
                if (tablet.getX() < Dimension.PIXELS_AMOUNT_X - 4) {
                    tablet.setX(tablet.getX() + 1);
                }
                break;
            case AB:
                moveBall();
                break;
            default:
                break;
        }
    }

    @Override
    public void updatePixels() {
        Screen.updatePixelsInBlock(brick.getBlock(), brick.getX(), brick.getY());
        Screen.updatePixelsInBlock(ball.getBlock(), ball.getX(), ball.getY());
        Screen.updatePixelsInBlock(tablet.getBlock(), tablet.getX(), tablet.getY());
        Screen.updateAuxiliaryPixelsInLine(getExtraLife());
    }
}

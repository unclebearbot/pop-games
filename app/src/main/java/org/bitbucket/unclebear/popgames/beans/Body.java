package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.utils.Resource;
import org.bitbucket.unclebear.popgames.utils.Setting;

public class Body {
    private static String[] skinNames;
    private static int[] skins;
    private static int skinIndex;
    private static int skinId;
    private static Bitmap background;
    private static Paint backgroundPaint;
    private static Matrix backgroundMatrix;
    private static Border upBorder;
    private static Border downBorder;
    private static Border leftBorder;
    private static Border rightBorder;

    private Body() {
    }

    public static void initialize(int x, int y, int width, int height) {
        if (background != null) {
            return;
        }
        final int skinAmount = 6;
        skins = new int[skinAmount];
        for (int i = 0; i < skinAmount; ++i) {
            skins[i] = R.drawable.body_01 + i;
        }
        skinNames = new String[skinAmount];
        for (int i = 0; i < skinAmount; ++i) {
            skinNames[i] = Resource.getString(R.string.skin_name_1 + i);
        }
        Resource.setBitmapsInPool(skins);
        skinIndex = Setting.get(Setting.SKIN, 0);
        background = Resource.getBitmap(skins[skinIndex]);
        backgroundPaint = new Paint();
        backgroundMatrix = new Matrix();
        int skinWidth = background.getWidth();
        final float scaleLevelFactor = (float) width / skinWidth;
        backgroundMatrix.setScale(scaleLevelFactor, scaleLevelFactor);
        backgroundMatrix.postTranslate(x, y);
        final float spacingFactor = 0.02f;
        int spacing = (int) (width * spacingFactor);
        spacing = spacing < 2 ? 2 : spacing;
        upBorder = new Border(true, x, y, width, spacing, true, true);
        downBorder = new Border(true, x, y + height - spacing, width, spacing, false, false);
        leftBorder = new Border(true, x, y, spacing, height, true, false);
        rightBorder = new Border(true, x + width - spacing, y, spacing, height, false, true);
    }

    public static String[] getSkinNames() {
        return skinNames == null ? new String[]{} : skinNames;
    }

    public static int getSkinIndex() {
        return skinIndex;
    }

    public static void setSkinIndex(int index) {
        skinIndex = index;
        Setting.set(Setting.SKIN, skinIndex);
        if (0 <= skinIndex && skinIndex < skins.length && (skinId != skins[skinIndex] || background == null)) {
            background = Resource.getBitmap(skins[skinIndex]);
            skinId = skins[skinIndex];
        }
    }

    public static void draw(Canvas canvas) {
        canvas.drawBitmap(background, backgroundMatrix, backgroundPaint);
        upBorder.draw(canvas);
        downBorder.draw(canvas);
        leftBorder.draw(canvas);
        rightBorder.draw(canvas);
    }
}

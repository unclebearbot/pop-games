package org.bitbucket.unclebear.popgames.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;

import java.util.HashMap;

public class Resource {
    private static Context context;
    private static Resources resources;
    private static Handler handler;
    private static HashMap<Integer, Bitmap> bitmapPool;
    private static int[] bitmapsInPool;
    private static HashMap<Integer, Bitmap> bitmapFallbackPool;

    private Resource() {
    }

    public static void initialize(Context context) {
        if (Resource.context != null) {
            return;
        }
        Resource.context = context;
        resources = context.getResources();
        handler = new Handler();
        bitmapPool = new HashMap<>();
        bitmapFallbackPool = new HashMap<>();
    }

    public static Context getContext() {
        return context;
    }

    public static Resources getResources() {
        return resources;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static Bitmap getBitmap(int id) {
        if (bitmapPool == null || !bitmapPool.containsKey(id)) {
            return getFallbackBitmap(id);
        }
        return bitmapPool.get(id);
    }

    public static Bitmap getFallbackBitmap(int id) {
        if (bitmapFallbackPool == null) {
            return BitmapFactory.decodeResource(resources, id);
        }
        if (!bitmapFallbackPool.containsKey(id)) {
            bitmapFallbackPool.put(id, BitmapFactory.decodeResource(resources, id));
        }
        return bitmapFallbackPool.get(id);
    }

    public static void setBitmapsInPool(int[] ids) {
        bitmapsInPool = ids;
    }

    public static void inflateBitmapPool(int index) {
        if (bitmapPool == null || bitmapsInPool == null || index < 0 || index >= bitmapsInPool.length) {
            return;
        }
        new Thread(() -> {
            int right = index + 1 < bitmapsInPool.length ? index + 1 : 0;
            if (!bitmapPool.containsKey(bitmapsInPool[right])) {
                bitmapPool.put(bitmapsInPool[right], BitmapFactory.decodeResource(resources, bitmapsInPool[right]));
            }
            int left = index - 1 > 0 ? index - 1 : bitmapsInPool.length - 1;
            if (!bitmapPool.containsKey(bitmapsInPool[left])) {
                bitmapPool.put(bitmapsInPool[left], BitmapFactory.decodeResource(resources, bitmapsInPool[left]));
            }
        }).start();
    }

    public static void deflateBitmapPool() {
        if (bitmapPool == null) {
            return;
        }
        bitmapPool.clear();
        bitmapFallbackPool.clear();
    }

    public static void deflateBitmapPoolAround(int index) {
        if (bitmapPool == null || bitmapsInPool == null || index < 0 || index >= bitmapsInPool.length) {
            return;
        }
        new Thread(() -> {
            int left = index - 1 > 0 ? index - 1 : bitmapsInPool.length - 1;
            if (bitmapPool.containsKey(bitmapsInPool[left])) {
                bitmapPool.remove(bitmapsInPool[left]);
            }
            int right = index + 1 < bitmapsInPool.length ? index + 1 : 0;
            if (bitmapPool.containsKey(bitmapsInPool[right])) {
                bitmapPool.remove(bitmapsInPool[right]);
            }
        }).start();
    }

    public static String getString(int id) {
        if (context == null) {
            return "";
        }
        return context.getString(id);
    }
}

package org.bitbucket.unclebear.popgames.games.racing;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.controllers.Sound;
import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Mathematics;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

import java.util.ArrayList;

public class RacingGame extends Game {
    private Road road;
    private Car hero;
    private ArrayList<Car> enemies;

    public RacingGame() {
        setLevelUpScoreBase(50);
        setLevelUpScoreInterval(20);
    }

    private void moveRoad() {
        road.moveOneStep();
    }

    private void moveEnemies() {
        if (!hero.isAlive()) {
            terminate();
            Screen.clearPixels();
            start();
        }
        while (enemies.size() < 2) {
            enemies.add(getRandomEnemy());
            Sound.play(R.raw.effect_negative);
        }
        for (Car enemy : enemies) {
            if (!enemy.isAlive()) {
                enemies.remove(enemy);
                break;
            }
            int overtakingOffset = enemy.getY() > hero.getY() ? 1 : 0;
            Screen.updatePixelsInRect(false, new int[]{enemy.getX(), enemy.getY() - 1 + overtakingOffset, enemy.getBlock().length, 2 + overtakingOffset});
            enemy.enemyMove(this);
            if (enemy.isEnemyCollided(hero)) {
                hero.setAlive(false);
                finallyDecreaseLife();
                return;
            }
        }
    }

    private Car getRandomEnemy() {
        return new Car(false, (int) (1 + 3 * Mathematics.getRandomBetween(0, 2)), -4);
    }

    @Override
    public void start() {
        road = new Road(0, 0);
        hero = new Car(true, 1, Dimension.PIXELS_AMOUNT_Y - 5);
        enemies = new ArrayList<>();
        enemies.add(new Car(false, (int) (1 + 3 * Mathematics.getRandomBetween(0, 2)), -16));
        enemies.add(new Car(false, (int) (1 + 3 * Mathematics.getRandomBetween(0, 2)), -4));
        resume();
    }

    @Override
    public void resume() {
        clearSleepyThreads();
        int baseSpeed = 4;
        addSleepyThread(new SleepyThread((int) (Dimension.SECOND / (baseSpeed * (5 + Hud.getSpeed() / 2f))), this::moveRoad));
        addSleepyThread(new SleepyThread((int) (Dimension.SECOND / (baseSpeed * 2)), hero::heroBeat));
        addSleepyThread(new SleepyThread((int) (Dimension.SECOND / (baseSpeed * (1 + Hud.getSpeed() / 2f))), this::moveEnemies));
        startAllSleepyThreads();
    }

    @Override
    public void pressButton(Button.Key key) {
        switch (key) {
            case LEFT:
                heroMoveLeft();
                break;
            case RIGHT:
                heroMoveRight();
                break;
            case AB:
                moveEnemies();
                break;
            default:
                break;
        }
    }

    private void heroMoveLeft() {
        if (hero.getX() >= 4) {
            Sound.play(R.raw.effect_click);
            int x = hero.getX();
            hero.setX(hero.getX() - 3);
            Screen.updatePixelsInRect(false, new int[]{x, hero.getY(), hero.getBlock().length, hero.getBlock()[0].length});
        }
    }

    private void heroMoveRight() {
        if (hero.getX() < Dimension.PIXELS_AMOUNT_X - 5) {
            Sound.play(R.raw.effect_click);
            int x = hero.getX();
            hero.setX(hero.getX() + 3);
            Screen.updatePixelsInRect(false, new int[]{x, hero.getY(), hero.getBlock().length, hero.getBlock()[0].length});
        }
    }

    @Override
    public void updatePixels() {
        Screen.updatePixelsInBlock(road.getBlock(), road.getX(), road.getY());
        Screen.updatePixelsInBlock(hero.getBlock(), hero.getX(), hero.getY());
        for (Car enemy : enemies) {
            Screen.updatePixelsInBlock(enemy.getBlock(), enemy.getX(), enemy.getY());
        }
        Screen.updateAuxiliaryPixelsInLine(getExtraLife());
    }
}

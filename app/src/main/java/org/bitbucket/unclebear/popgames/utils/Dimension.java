package org.bitbucket.unclebear.popgames.utils;

public class Dimension {
    public static final long NANOSECOND = 1;
    public static final long MICROSECOND = NANOSECOND * 1000;
    public static final long MILLISECOND = MICROSECOND * 1000;
    public static final long SECOND = MILLISECOND * 1000;
    public static final int UPDATE_FREQUENCY = 30;
    public static final int LCD_ON_ALPHA = 160;
    public static final int LCD_OFF_ALPHA = 8;
    public static final int BORDER_DARK_ALPHA = 255;
    public static final int BORDER_LIGHT_ALPHA = 64;
    public static final int PIXELS_AMOUNT_X = 10;
    public static final int PIXELS_AMOUNT_Y = 20;
    public static final int AUXILIARY_PIXEL_AMOUNT = 4;
    public static final String ERROR = "ERROR";

    private Dimension() {
    }
}

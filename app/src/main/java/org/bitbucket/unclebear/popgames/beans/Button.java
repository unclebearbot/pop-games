package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.utils.Resource;

public class Button {
    private Key key;
    private int x;
    private int y;
    private int size;
    private Bitmap background;
    private Bitmap backgroundPressed;
    private Paint backgroundPaint;
    private Matrix backgroundMatrix;
    private Paint textPaint;
    private float textStrokeWidth;
    private boolean isPressed;

    public Button(Key key, int x, int y, int size) {
        this.key = key;
        this.x = x;
        this.y = y;
        this.size = size;
        switch (this.key) {
            case UP:
                background = Resource.getBitmap(R.drawable.button_up);
                backgroundPressed = Resource.getBitmap(R.drawable.button_up_pressed);
                break;
            case DOWN:
                background = Resource.getBitmap(R.drawable.button_down);
                backgroundPressed = Resource.getBitmap(R.drawable.button_down_pressed);
                break;
            case LEFT:
                background = Resource.getBitmap(R.drawable.button_left);
                backgroundPressed = Resource.getBitmap(R.drawable.button_left_pressed);
                break;
            case RIGHT:
                background = Resource.getBitmap(R.drawable.button_right);
                backgroundPressed = Resource.getBitmap(R.drawable.button_right_pressed);
                break;
            case AB:
                background = Resource.getBitmap(R.drawable.button_ab);
                backgroundPressed = Resource.getBitmap(R.drawable.button_ab_pressed);
                break;
            case MENU:
            case START:
                background = Resource.getBitmap(R.drawable.button_menu);
                backgroundPressed = Resource.getBitmap(R.drawable.button_menu_pressed);
                break;
            default:
                break;
        }
        backgroundPaint = new Paint();
        backgroundPaint.setAntiAlias(true);
        backgroundMatrix = new Matrix();
        final float scaleLevel = (float) this.size / background.getWidth();
        backgroundMatrix.setScale(scaleLevel, scaleLevel);
        backgroundMatrix.postTranslate(this.x, this.y);
        final float textSizeFactor = 0.25f;
        float textSize = size * textSizeFactor;
        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(textSize);
        textPaint.setTypeface(Typeface.MONOSPACE);
        final float textStrokeWidthFactor = 0.05f;
        textStrokeWidth = size * textStrokeWidthFactor;
        textStrokeWidth = textStrokeWidth < 1 ? 1 : textStrokeWidth;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(isPressed ? backgroundPressed : background, backgroundMatrix, backgroundPaint);
        if (key == Key.MENU) {
            textPaint.setStrokeWidth(textStrokeWidth);
            textPaint.setColor(Color.LTGRAY);
            canvas.drawText(Resource.getString(R.string.select), (float) x + size / 2, (float) y + size, textPaint);
            textPaint.setStrokeWidth(0);
            textPaint.setColor(Color.DKGRAY);
            canvas.drawText(Resource.getString(R.string.select), (float) x + size / 2, (float) y + size, textPaint);
        } else if (key == Key.START) {
            textPaint.setStrokeWidth(textStrokeWidth);
            textPaint.setColor(Color.LTGRAY);
            canvas.drawText(Resource.getString(R.string.start), (float) x + size / 2, (float) y + size, textPaint);
            textPaint.setStrokeWidth(0);
            textPaint.setColor(Color.DKGRAY);
            canvas.drawText(Resource.getString(R.string.start), (float) x + size / 2, (float) y + size, textPaint);
        }
    }

    public Key getKey() {
        return key;
    }

    public boolean isInArea(int pressedX, int pressedY) {
        return x < pressedX && pressedX < x + size && y < pressedY && pressedY < y + size;
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public enum Key {UP, DOWN, LEFT, RIGHT, AB, MENU, START}
}

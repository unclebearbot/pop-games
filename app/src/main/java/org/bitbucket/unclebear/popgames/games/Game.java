package org.bitbucket.unclebear.popgames.games;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.controllers.Gaming;
import org.bitbucket.unclebear.popgames.controllers.Sound;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

import java.util.ArrayList;
import java.util.List;

public abstract class Game {
    private List<SleepyThread> sleepyThreads = new ArrayList<>();
    private int extraLife = 2;
    private int levelUpScoreBase = 0;
    private int levelUpScoreInterval = 0;

    public abstract void start();

    public void pause() {
        stopAllSleepyThreads();
        clearSleepyThreads();
    }

    public abstract void resume();

    public void terminate() {
        stopAllSleepyThreads();
        clearSleepyThreads();
    }

    public abstract void pressButton(Button.Key key);

    public abstract void updatePixels();

    public int getExtraLife() {
        return extraLife;
    }

    public void setExtraLife(int extraLife) {
        this.extraLife = extraLife;
    }

    public List<SleepyThread> getSleepyThreads() {
        return sleepyThreads;
    }

    public void startAllSleepyThreads() {
        if (sleepyThreads == null) {
            return;
        }
        for (SleepyThread sleepyThread : sleepyThreads) {
            sleepyThread.start();
        }
    }

    public void stopAllSleepyThreads() {
        if (sleepyThreads == null) {
            return;
        }
        for (SleepyThread sleepyThread : sleepyThreads) {
            sleepyThread.terminate();
        }
    }

    public void setIntervalToAllSleepyThreads(long interval) {
        if (sleepyThreads == null) {
            return;
        }
        for (SleepyThread sleepyThread : sleepyThreads) {
            sleepyThread.setInterval(interval);
        }
    }

    public void clearSleepyThreads() {
        if (sleepyThreads == null) {
            return;
        }
        sleepyThreads.clear();
    }

    public void addSleepyThread(SleepyThread sleepyThread) {
        if (sleepyThreads == null) {
            return;
        }
        sleepyThreads.add(sleepyThread);
    }

    public void increaseScore(int i) {
        Hud.setScore(Hud.getScore() + i);
        if (levelUpScoreBase != 0 && Hud.getScore() >= levelUpScoreBase) {
            levelUpScoreBase += levelUpScoreInterval;
            increaseLevel();
        }
    }

    public void increaseLevel() {
        Hud.setLevel(Hud.getLevel() + 1);
        if (Hud.getLevel() >= 10) {
            Hud.setLevel(Hud.BASE_SPEED_AND_LEVEL);
            increaseSpeed();
        }
    }

    public void increaseSpeed() {
        Hud.setSpeed(Hud.getSpeed() + 1);
        if (Hud.getSpeed() % 5 == 0) {
            increaseLife();
        }
    }

    public void increaseLife() {
        Sound.play(R.raw.effect_reward);
        setExtraLife(getExtraLife() + 1);
    }

    public void finallyDecreaseLife() {
        if (getExtraLife() > 0) {
            Sound.play(R.raw.effect_alert);
            setExtraLife(getExtraLife() - 1);
        } else {
            Gaming.gameOver();
        }
    }

    public int getLevelUpScoreBase() {
        return levelUpScoreBase;
    }

    public void setLevelUpScoreBase(int levelUpScoreBase) {
        this.levelUpScoreBase = levelUpScoreBase;
    }

    public int getLevelUpScoreInterval() {
        return levelUpScoreInterval;
    }

    public void setLevelUpScoreInterval(int levelUpScoreInterval) {
        this.levelUpScoreInterval = levelUpScoreInterval;
    }
}

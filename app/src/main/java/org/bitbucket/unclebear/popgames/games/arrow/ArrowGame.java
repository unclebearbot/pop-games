package org.bitbucket.unclebear.popgames.games.arrow;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.controllers.Sound;
import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.games.Role;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

public class ArrowGame extends Game {
    private Hero hero;
    private Enemy enemy;
    private Arrow[] arrows;
    private int maxArrows = 50;
    private int arrowsIndex = 0;

    public ArrowGame() {
        setLevelUpScoreBase(10);
        setLevelUpScoreInterval(10);
    }

    private void shoot() {
        addArrow(false);
        if (hero.isPromoted()) {
            addArrow(true);
        }
    }

    private void addArrow(boolean isDouble) {
        if (arrows[arrowsIndex] == null) {
            arrows[arrowsIndex] = new Arrow(hero.getX() + (isDouble ? 4 : 2), hero.getY());
        } else {
            arrows[arrowsIndex].setCollided(false);
            arrows[arrowsIndex].setX(hero.getX() + (isDouble ? 4 : 2));
            arrows[arrowsIndex].setY(hero.getY());
        }
        ++arrowsIndex;
        arrowsIndex = arrowsIndex >= maxArrows ? 0 : arrowsIndex;
    }

    private void moveEnemies() {
        Sound.play(R.raw.effect_click);
        if (!enemy.moveDownWithoutCollisions()) {
            enemy.clear();
            hero.setPromoted(false);
            finallyDecreaseLife();
        }
    }

    private void moveArrows() {
        for (Role arrow : arrows) {
            if (arrow == null || ((Arrow) arrow).isCollided()) {
                continue;
            }
            arrow.setY(arrow.getY() - 1);
            if (arrow.getY() < 0) {
                ((Arrow) arrow).setCollided(true);
                enemy.remove(arrow.getX(), arrow.getY());
            }
            if (enemy.isInBlockAndUp(arrow.getX(), arrow.getY())) {
                ((Arrow) arrow).setCollided(true);
                enemy.remove(arrow.getX(), arrow.getY());
                increaseScore(1);
            }
        }
    }

    @Override
    public void start() {
        Screen.clearPixels();
        hero = new Hero(Dimension.PIXELS_AMOUNT_X / 2 - 3, Dimension.PIXELS_AMOUNT_Y - 2);
        enemy = new Enemy(0, 0);
        arrows = new Arrow[maxArrows];
        resume();
    }
    @Override
    public void resume() {
        clearSleepyThreads();
        int arrowSpeedFactor = 100;
        addSleepyThread(new SleepyThread(Dimension.SECOND / arrowSpeedFactor, this::moveArrows));
        int enemiesBaseSpeedFactor = 1;
        addSleepyThread(new SleepyThread((int) (Dimension.SECOND / (enemiesBaseSpeedFactor * (1 + Hud.getSpeed() / 10f))), this::
                moveEnemies));
        startAllSleepyThreads();
    }

    @Override
    public void pressButton(Button.Key key) {
        switch (key) {
            case UP:
                promoteHero();
                break;
            case DOWN:
                moveEnemies();
                break;
            case LEFT:
                moveHeroLeft();
                break;
            case RIGHT:
                moveHeroRight();
                break;
            case AB:
                shoot();
                break;
            default:
                break;
        }
    }

    private void promoteHero() {
        if (!hero.isPromoted() && getExtraLife() > 0) {
            hero.setPromoted(true);
            finallyDecreaseLife();
        }
    }

    private void moveHeroLeft() {
        if (hero.getX() >= -1) {
            hero.setX(hero.getX() - 1);
        }
    }

    private void moveHeroRight() {
        if (hero.getX() < Dimension.PIXELS_AMOUNT_X - (hero.isPromoted() ? 5 : 3)) {
            hero.setX(hero.getX() + 1);
        }
    }

    @Override
    public void updatePixels() {
        Screen.updatePixelsInBlock(hero.getBlock(), hero.getX(), hero.getY());
        Screen.updatePixelsInBlock(enemy.getBlock(), enemy.getX(), enemy.getY());
        for (Role arrow : arrows) {
            if (arrow == null || ((Arrow) arrow).isCollided()) {
                continue;
            }
            Screen.updatePixels(true, new int[][]{{arrow.getX(), arrow.getY()}});
        }
        Screen.updateAuxiliaryPixelsInLine(getExtraLife());
    }
}

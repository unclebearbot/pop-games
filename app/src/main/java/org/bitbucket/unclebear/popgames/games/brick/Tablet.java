package org.bitbucket.unclebear.popgames.games.brick;

import org.bitbucket.unclebear.popgames.games.Role;

class Tablet extends Role {
    Tablet(int x, int y) {
        super(x, y);
        setBlock(new boolean[][]{{false}, {true}, {true}, {true}, {false}});
    }
}

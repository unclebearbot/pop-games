package org.bitbucket.unclebear.popgames.games.tetris.shapes;

import org.bitbucket.unclebear.popgames.games.tetris.Shape;

public class JShape extends Shape {

    public JShape(int x, int y) {
        super(x, y);
        boolean[][] block = new boolean[][]{{true, true}, {false, true}, {false, true}};
        setBlock(block);
    }
}

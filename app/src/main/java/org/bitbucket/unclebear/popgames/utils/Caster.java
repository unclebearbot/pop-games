package org.bitbucket.unclebear.popgames.utils;

public class Caster<T> {
    @SuppressWarnings("unchecked")
    public T cast(Object object) {
        return (T) (object == null ? new Object() : object);
    }
}

package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import org.bitbucket.unclebear.popgames.utils.Dimension;

public class Screen {
    private static Pixel[][] pixels;
    private static Pixel[][] auxiliaryPixels;
    private static boolean shouldShowContent = true;
    private static Rect background;
    private static Paint backgroundPaint;
    private static Border upBorder;
    private static Border downBorder;
    private static Border leftBorder;
    private static Border rightBorder;
    private static Rect pixelsBox;
    private static Paint boxPaint;

    private Screen() {
    }

    public static void initialize(int x, int y, int width, int height) {
        if (pixels != null) {
            return;
        }
        background = new Rect(x, y, x + width, y + height);
        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.LTGRAY);
        backgroundPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        final float spacingFactor = 0.0125f;
        int spacing = (int) (width * spacingFactor);
        spacing = spacing < 3 ? 3 : spacing;
        upBorder = new Border(false, x, y, width, spacing, true, true);
        downBorder = new Border(false, x, y + height - spacing, width, spacing, false, false);
        leftBorder = new Border(false, x, y, spacing, height, true, false);
        rightBorder = new Border(false, x + width - spacing, y, spacing, height, false, true);
        pixels = new Pixel[Dimension.PIXELS_AMOUNT_X][Dimension.PIXELS_AMOUNT_Y];
        auxiliaryPixels = new Pixel[Dimension.AUXILIARY_PIXEL_AMOUNT][Dimension.AUXILIARY_PIXEL_AMOUNT];
        final float pixelSizeFactor = 0.045f;
        int pixelSize = (int) (height * pixelSizeFactor);
        int pixelsOffset = (height - pixelSize * Dimension.PIXELS_AMOUNT_Y) / 2;
        for (int i = 0; i < Dimension.PIXELS_AMOUNT_X; ++i) {
            for (int j = 0; j < Dimension.PIXELS_AMOUNT_Y; ++j) {
                pixels[i][j] = new Pixel(x + pixelsOffset + pixelSize * i, y + pixelsOffset + pixelSize * j, pixelSize);
            }
        }
        final float auxiliaryPixelSizeFactor = 0.03f;
        final float auxiliaryPixelsOffsetXFactor = 0.75f;
        final float auxiliaryPixelsOffsetYFactor = 0.3f;
        int auxiliaryPixelSize = (int) (height * auxiliaryPixelSizeFactor);
        int auxiliaryPixelsOffsetX = (int) (width * auxiliaryPixelsOffsetXFactor);
        int auxiliaryPixelsOffsetY = (int) (height * auxiliaryPixelsOffsetYFactor);
        for (int i = 0; i < Dimension.AUXILIARY_PIXEL_AMOUNT; ++i) {
            for (int j = 0; j < Dimension.AUXILIARY_PIXEL_AMOUNT; ++j) {
                auxiliaryPixels[i][j] = new Pixel(x + auxiliaryPixelsOffsetX + auxiliaryPixelSize * i, y + auxiliaryPixelsOffsetY + auxiliaryPixelSize * j, auxiliaryPixelSize);
            }
        }
        pixelsBox = new Rect(pixels[0][0].getX() - spacing, pixels[0][0].getY() - spacing, pixels[Dimension.PIXELS_AMOUNT_X - 1][Dimension.PIXELS_AMOUNT_Y - 1].getX() + pixelSize + spacing * 2, pixels[Dimension.PIXELS_AMOUNT_X - 1][Dimension.PIXELS_AMOUNT_Y - 1].getY() + pixelSize + spacing * 2);
        boxPaint = new Paint();
        boxPaint.setColor(Color.DKGRAY);
        boxPaint.setAlpha(255);
        boxPaint.setStrokeWidth(spacing * 0.5f);
        boxPaint.setStyle(Paint.Style.STROKE);
        final float hudWidthFactor = 0.3f;
        Hud.initialize((int) (x + width * (1 - hudWidthFactor)), y, (int) (width * hudWidthFactor), height);
    }

    public static Pixel[][] getPixels() {
        if (pixels == null) {
            return new Pixel[Dimension.PIXELS_AMOUNT_X][Dimension.PIXELS_AMOUNT_Y];
        }
        return pixels;
    }

    public static void clearPixels() {
        if (pixels == null) {
            return;
        }
        for (Pixel[] line : pixels) {
            for (Pixel pixel : line) {
                pixel.lightUp(false);
            }
        }
    }

    public static void updatePixels(boolean willLightUp, int[][] dots) {
        if (pixels == null) {
            return;
        }
        updatePixelsWithOffset(willLightUp, 0, 0, dots);
    }

    public static void updatePixelsWithOffset(boolean willLightUp, int offsetX, int offsetY, int[][] dots) {
        if (pixels == null) {
            return;
        }
        for (int[] dot : dots) {
            int x = dot[0] + offsetX;
            int y = dot[1] + offsetY;
            boolean isDotInScreen = 0 <= x && x < Dimension.PIXELS_AMOUNT_X && 0 <= y && y < Dimension.PIXELS_AMOUNT_Y;
            if (dot.length == 2 && isDotInScreen) {
                pixels[x][y].lightUp(willLightUp);
            }
        }
    }

    public static void updatePixelsInRect(boolean willLightUp, int[] rect) {
        if (pixels == null) {
            return;
        }
        if (rect.length != 4) {
            return;
        }
        int x = rect[0];
        int y = rect[1];
        int width = rect[2];
        int height = rect[3];
        for (int i = x; i < x + width; ++i) {
            for (int j = y; j < y + height; ++j) {
                updatePixels(willLightUp, new int[][]{{i, j}});
            }
        }
    }

    public static void updatePixelsInBlock(boolean[][] block, int offsetX, int offsetY) {
        if (pixels == null || block == null) {
            return;
        }
        for (int i = 0; i < block.length; ++i) {
            for (int j = 0; j < block[i].length; ++j) {
                if (0 <= offsetX + i && offsetX + i < Dimension.PIXELS_AMOUNT_X && 0 <= offsetY + j && offsetY + j < Dimension.PIXELS_AMOUNT_Y) {
                    pixels[offsetX + i][offsetY + j].lightUp(block[i][j]);
                }
            }
        }
    }

    public static void updateAuxiliaryPixelsInBlock(boolean[][] block, int offsetX, int offsetY) {
        if (auxiliaryPixels == null || block == null) {
            return;
        }
        int x = offsetX + (Dimension.AUXILIARY_PIXEL_AMOUNT - block.length) / 2;
        int y = offsetY + (Dimension.AUXILIARY_PIXEL_AMOUNT - block[0].length) / 2;
        for (int i = 0; i < block.length; ++i) {
            for (int j = 0; j < block[i].length; ++j) {
                if (0 <= x + i && x + i < Dimension.AUXILIARY_PIXEL_AMOUNT && 0 <= y + j && y + j < Dimension.AUXILIARY_PIXEL_AMOUNT) {
                    auxiliaryPixels[x + i][y + j].lightUp(block[i][j]);
                }
            }
        }
    }

    public static boolean[][] getPixelsAsBlock() {
        boolean[][] result = new boolean[Dimension.PIXELS_AMOUNT_X][Dimension.PIXELS_AMOUNT_Y];
        if (pixels == null) {
            return result;
        }
        for (int i = 0; i < Dimension.PIXELS_AMOUNT_X; ++i) {
            for (int j = 0; j < Dimension.PIXELS_AMOUNT_Y; ++j) {
                result[i][j] = pixels[i][j].isUp();
            }
        }
        return result;
    }

    public static void lightUpPixelsAsBlockTrueDots(boolean[][] block, int offsetX, int offsetY) {
        if (pixels == null || block == null) {
            return;
        }
        for (int i = 0; i < block.length; ++i) {
            for (int j = 0; j < block[i].length; ++j) {
                lightUpDotAsBlockTrueDots(block, i, j, offsetX, offsetY);
            }
        }
    }

    private static void lightUpDotAsBlockTrueDots(boolean[][] block, int i, int j, int offsetX, int offsetY) {
        boolean xInRange = 0 <= offsetX + i && offsetX + i < Dimension.PIXELS_AMOUNT_X;
        boolean yInRange = 0 <= offsetY + j && offsetY + j < Dimension.PIXELS_AMOUNT_Y;
        if (xInRange && yInRange && block[i][j]) {
            pixels[offsetX + i][offsetY + j].lightUp(true);
        }
    }

    public static void putOutUpPixelsInBlock(boolean[][] block, int offsetX, int offsetY) {
        if (pixels == null || block == null) {
            return;
        }
        for (int i = 0; i < block.length; ++i) {
            for (int j = 0; j < block[i].length; ++j) {
                putOut(block, offsetX, offsetY, i, j);
            }
        }
    }

    private static void putOut(boolean[][] block, int offsetX, int offsetY, int i, int j) {
        boolean xInRange = 0 <= offsetX + i && offsetX + i < Dimension.PIXELS_AMOUNT_X;
        boolean yInRange = 0 <= offsetY + j && offsetY + j < Dimension.PIXELS_AMOUNT_Y;
        if (xInRange && yInRange && block[i][j]) {
            pixels[offsetX + i][offsetY + j].lightUp(false);
        }
    }

    public static void reversePixels(int x1, int y1, int x2, int y2) {
        if (pixels == null) {
            return;
        }
        for (int i = x1; i < x2; ++i) {
            for (int j = y1; j < y2; ++j) {
                pixels[i][j].reverse();
            }
        }
    }

    public static Pixel[][] getAuxiliaryPixels() {
        if (auxiliaryPixels == null) {
            return new Pixel[Dimension.AUXILIARY_PIXEL_AMOUNT][Dimension.AUXILIARY_PIXEL_AMOUNT];
        }
        return auxiliaryPixels;
    }

    public static void clearAuxiliaryPixels() {
        if (auxiliaryPixels == null) {
            return;
        }
        for (Pixel[] line : auxiliaryPixels) {
            for (Pixel pixel : line) {
                pixel.lightUp(false);
            }
        }
    }

    public static void updateAuxiliaryPixelsInLine(int line) {
        if (auxiliaryPixels == null) {
            return;
        }
        int tempLine = line;
        tempLine = tempLine < 0 ? 0 : tempLine;
        tempLine = tempLine > Dimension.AUXILIARY_PIXEL_AMOUNT ? Dimension.AUXILIARY_PIXEL_AMOUNT : tempLine;
        tempLine = Dimension.AUXILIARY_PIXEL_AMOUNT - tempLine;
        for (int i = 0; i < Dimension.AUXILIARY_PIXEL_AMOUNT; ++i) {
            Pixel[] pixels = auxiliaryPixels[i];
            for (int j = 0; j < Dimension.AUXILIARY_PIXEL_AMOUNT; ++j) {
                pixels[j].lightUp(j >= tempLine);
            }
        }
    }

    public static void setShouldShowContent(boolean shouldShowContent) {
        Screen.shouldShowContent = shouldShowContent;
    }

    public static boolean isOutOfBound(int x, int y) {
        return x < 0 || x >= Dimension.PIXELS_AMOUNT_X || y < 0 || y >= Dimension.PIXELS_AMOUNT_Y;
    }

    public static void draw(Canvas canvas) {
        if (pixels == null) {
            return;
        }
        canvas.drawRect(background, backgroundPaint);
        upBorder.draw(canvas);
        downBorder.draw(canvas);
        leftBorder.draw(canvas);
        rightBorder.draw(canvas);
        if (shouldShowContent) {
            drawPixels(canvas);
            canvas.drawRect(pixelsBox, boxPaint);
            Hud.draw(canvas);
        }
    }

    private static void drawPixels(Canvas canvas) {
        for (Pixel[] line : pixels) {
            for (Pixel pixel : line) {
                pixel.draw(canvas);
            }
        }
        for (Pixel[] line : auxiliaryPixels) {
            for (Pixel pixel : line) {
                pixel.draw(canvas);
            }
        }
    }

    public static boolean isAnyUpInBlockUpDots(boolean[][] block, int offsetX, int offsetY) {
        if (pixels == null) {
            return false;
        }
        for (int i = 0; i < block.length; ++i) {
            for (int j = 0; j < block[i].length; ++j) {
                if (block[i][j] && isUp(offsetX + i, offsetY + j)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isAnyUp(int[][] dots) {
        if (pixels == null) {
            return false;
        }
        for (int[] dot : dots) {
            if (isUp(dot[0], dot[1])) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAllUp(int[][] dots) {
        if (pixels == null) {
            return false;
        }
        for (int[] dot : dots) {
            if (!isUp(dot[0], dot[1])) {
                return false;
            }
        }
        return true;
    }

    private static boolean isUp(int x, int y) {
        return !isOutOfBound(x, y) && pixels[x][y].isUp();
    }
}

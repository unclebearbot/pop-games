package org.bitbucket.unclebear.popgames.controllers;

import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Menu;
import org.bitbucket.unclebear.popgames.beans.Pad;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

import java.util.List;

public class Stage {
    private static final long BUTTON_FREQUENCY = 8;
    private static Status status = Status.CHAPTER;
    private static Status lastStatus;
    private static SleepyThread sleepyThread;
    private static long lastButtonPressedTimestamp;
    private static boolean isAfterMenuButtonFirstPressed;
    private static boolean isAfterStartButtonFirstPressed;

    private Stage() {
    }

    public static void start() {
        if (sleepyThread != null && sleepyThread.isRunning()) {
            return;
        }
        final long buttonInterval = Dimension.SECOND / Dimension.UPDATE_FREQUENCY;
        sleepyThread = new SleepyThread(buttonInterval, Stage::readButtons);
        sleepyThread.start();
        if (status == Status.CHAPTER) {
            Chapter.start();
        }
    }

    public static void terminate() {
        if (sleepyThread == null) {
            return;
        }
        sleepyThread.terminate();
        if (status == Status.GAMING) {
            status = Status.PAUSED;
            Pausing.pause();
        }
    }

    public static void reset() {
        status = Status.CHAPTER;
        Menu.terminate();
        Pausing.terminate();
        Gaming.terminate();
        Hud.reset();
        Chapter.reset();
    }

    public static void gameOver(char chapter) {
        Pausing.terminate();
        Chapter.setCurrentChapter(chapter);
        Chapter.setMission(1);
        status = Status.CHAPTER;
        Chapter.start();
    }

    public static Status getStatus() {
        return status;
    }

    public static void setStatus(Status status) {
        Stage.status = status;
    }

    private static void readButtons() {
        List<Button.Key> keys = Pad.getPressedButtonsKeys();
        if (keys.isEmpty()) {
            isAfterMenuButtonFirstPressed = false;
            isAfterStartButtonFirstPressed = false;
            return;
        }
        long currentButtonPressedTimestamp = System.nanoTime();
        long buttonInterval = Dimension.SECOND / BUTTON_FREQUENCY;
        if (currentButtonPressedTimestamp - lastButtonPressedTimestamp >= buttonInterval || lastButtonPressedTimestamp == 0) {
            lastButtonPressedTimestamp = currentButtonPressedTimestamp;
            processKeys(keys);
        }
    }

    private static void processKeys(List<Button.Key> keys) {
        for (Button.Key key : keys) {
            if (keys.contains(Button.Key.MENU)) {
                processMenuKey();
                return;
            } else if (keys.contains(Button.Key.START)) {
                processStartKey();
                return;
            } else {
                isAfterMenuButtonFirstPressed = false;
                isAfterStartButtonFirstPressed = false;
            }
            pressButton(key);
        }
    }

    private static void processMenuKey() {
        if (!isAfterMenuButtonFirstPressed) {
            pressButton(Button.Key.MENU);
            isAfterMenuButtonFirstPressed = true;
        }
    }

    private static void processStartKey() {
        if (!isAfterStartButtonFirstPressed) {
            pressButton(Button.Key.START);
            isAfterStartButtonFirstPressed = true;
        }
    }

    private static void pressButton(Button.Key key) {
        MyVibrator.vibrate();
        switch (key) {
            case MENU:
                pressMenuButton();
                break;
            case START:
                pressStartButton();
                break;
            case UP:
            case DOWN:
            case LEFT:
            case RIGHT:
            case AB:
                pressGamingButtons(key);
                break;
            default:
                break;
        }
    }

    private static void pressMenuButton() {
        if (isAfterMenuButtonFirstPressed) {
            return;
        }
        if (lastStatus == null) {
            lastStatus = status;
        }
        if (status == Status.MENU) {
            status = lastStatus;
            if (status == Status.GAMING) {
                Pausing.resume();
            }
            Menu.terminate();
        } else {
            lastStatus = status;
            if (status == Status.GAMING) {
                Pausing.pause();
            }
            status = Status.MENU;
            Menu.start();
        }
    }

    private static void pressStartButton() {
        if (isAfterStartButtonFirstPressed) {
            return;
        }
        if (status == Status.PAUSED) {
            status = Status.GAMING;
            Pausing.resume();
        } else if (status == Status.GAMING) {
            status = Status.PAUSED;
            Pausing.pause();
        } else if (status == Status.CHAPTER) {
            status = Status.GAMING;
            Chapter.gameBeginning();
        }
    }

    private static void pressGamingButtons(Button.Key key) {
        switch (status) {
            case CHAPTER:
                Chapter.pressButton(key);
                break;
            case GAMING:
                Gaming.pressButton(key);
                break;
            case MENU:
                Menu.pressButton(key);
                break;
            default:
                break;
        }
    }

    public enum Status {CHAPTER, GAMING, PAUSED, MENU}
}

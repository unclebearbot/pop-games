package org.bitbucket.unclebear.popgames.games.tetris;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.controllers.Sound;
import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.games.tetris.shapes.IShape;
import org.bitbucket.unclebear.popgames.games.tetris.shapes.JShape;
import org.bitbucket.unclebear.popgames.games.tetris.shapes.LShape;
import org.bitbucket.unclebear.popgames.games.tetris.shapes.OShape;
import org.bitbucket.unclebear.popgames.games.tetris.shapes.SShape;
import org.bitbucket.unclebear.popgames.games.tetris.shapes.TShape;
import org.bitbucket.unclebear.popgames.games.tetris.shapes.ZShape;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Mathematics;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

public class TetrisGame extends Game {
    private Shape currentShape;
    private Shape nextShape;
    private int baseSpeed = 2;

    public TetrisGame() {
        setExtraLife(0);
        setLevelUpScoreBase(10);
        setLevelUpScoreInterval(10);
    }

    private Shape getRandomShape() {
        int x = Dimension.PIXELS_AMOUNT_X / 2 - 2;
        int y = 0;
        int randomShapeIndex = (int) Mathematics.getRandomBetween(0, 9);
        Shape result = new OShape(x, y);
        while (randomShapeIndex >= 7) {
            randomShapeIndex -= 7;
        }
        switch (randomShapeIndex) {
            case 0:
                result = new IShape(x, y);
                break;
            case 1:
                result = new JShape(x, y);
                break;
            case 2:
                result = new LShape(x, y);
                break;
            case 3:
                result = new OShape(x, y);
                break;
            case 4:
                result = new SShape(x, y);
                break;
            case 5:
                result = new TShape(x, y);
                break;
            case 6:
                result = new ZShape(x, y);
                break;
            default:
                break;
        }
        return result;
    }

    @Override
    public void start() {
        currentShape = getRandomShape();
        nextShape = getRandomShape();
        Screen.lightUpPixelsAsBlockTrueDots(currentShape.getBlock(), currentShape.getX(), currentShape.getY());
        Screen.updateAuxiliaryPixelsInBlock(nextShape.getBlock(), 0, 0);
        resume();
    }

    @Override
    public void resume() {
        stopAllSleepyThreads();
        clearSleepyThreads();
        addSleepyThread(new SleepyThread((int) (Dimension.SECOND / (baseSpeed * (Hud.getSpeed() / 4f))), this::moveShapeDown));
        startAllSleepyThreads();
    }

    @Override
    public void pressButton(Button.Key key) {
        switch (key) {
            case DOWN:
            case LEFT:
            case RIGHT:
            case UP:
                moveShape(key);
                break;
            case AB:
                moveShapeDownToBottom();
                break;
            default:
                break;
        }
    }

    private boolean moveShapeDown() {
        boolean moved = moveShape(Button.Key.DOWN);
        if (!moved) {
            int removedLines = removeFullLines();
            Sound.play(removedLines > 0 ? R.raw.effect_positive : R.raw.effect_negative);
            increaseScoreByRemovedLines(removedLines);
            currentShape = nextShape;
            nextShape = getRandomShape();
            Screen.clearAuxiliaryPixels();
            if (failCreatingShape()) {
                setExtraLife(0);
                finallyDecreaseLife();
            } else {
                Screen.lightUpPixelsAsBlockTrueDots(currentShape.getBlock(), currentShape.getX(), currentShape.getY());
                Screen.updateAuxiliaryPixelsInBlock(nextShape.getBlock(), 0, 0);
                setIntervalToAllSleepyThreads((int) (Dimension.SECOND / (baseSpeed * (Hud.getSpeed() / 4f))));
            }
        }
        return moved;
    }

    private boolean moveShapeDownToBottom() {
        while (moveShapeDown()) {
            // nothing to do
        }
        return false;
    }

    private void increaseScoreByRemovedLines(int removedLines) {
        int score = 0;
        switch (removedLines) {
            case 1:
                score = 10;
                break;
            case 2:
                score = 20;
                break;
            case 3:
                score = 50;
                break;
            case 4:
                score = 100;
                break;
            default:
                break;
        }
        increaseScore(score);
    }

    private boolean failCreatingShape() {
        return Screen.isAnyUpInBlockUpDots(currentShape.getBlock(), currentShape.getX(), currentShape.getY());
    }

    private boolean moveShape(Button.Key key) {
        putOutCurrentShape();
        boolean result = currentShape.move(key);
        lightUpCurrentShape();
        return result;
    }

    private void putOutCurrentShape() {
        Screen.putOutUpPixelsInBlock(currentShape.getBlock(), currentShape.getX(), currentShape.getY());
    }

    private void lightUpCurrentShape() {
        Screen.lightUpPixelsAsBlockTrueDots(currentShape.getBlock(), currentShape.getX(), currentShape.getY());
    }

    private int removeFullLines() {
        int result = 0;
        boolean[][] block = Screen.getPixelsAsBlock();
        for (int j = 0; j < Dimension.PIXELS_AMOUNT_Y; ++j) {
            if (isFullLine(block, j)) {
                ++result;
                removeLine(block, j);
                moveBlockDownBefore(block, j);
            }
        }
        Screen.updatePixelsInBlock(block, 0, 0);
        return result;
    }

    private boolean isFullLine(boolean[][] block, int lineNumber) {
        for (int i = 0; i < Dimension.PIXELS_AMOUNT_X; ++i) {
            if (!block[i][lineNumber]) {
                return false;
            }
        }
        return true;
    }

    private void removeLine(boolean[][] block, int lineNumber) {
        for (int i = 0; i < Dimension.PIXELS_AMOUNT_X; ++i) {
            block[i][lineNumber] = false;
        }
    }

    private void moveBlockDownBefore(boolean[][] block, int lineNumber) {
        for (int j = lineNumber; j > 0; --j) {
            for (int i = 0; i < Dimension.PIXELS_AMOUNT_X; ++i) {
                block[i][j] = block[i][j - 1];
            }
        }
        removeLine(block, 0);
    }

    @Override
    public void updatePixels() {
        // there is no need to update pixels
    }
}

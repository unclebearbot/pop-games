package org.bitbucket.unclebear.popgames.utils;

public class Mathematics {
    private Mathematics() {
    }

    public static long getRandomBetween(long i, long j) {
        long range = i - j;
        return (i < j ? i : j) + Math.round(Math.random() * (range > 0 ? range : -range));
    }
}

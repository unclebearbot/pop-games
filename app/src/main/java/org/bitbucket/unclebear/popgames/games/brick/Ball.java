package org.bitbucket.unclebear.popgames.games.brick;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.controllers.Sound;
import org.bitbucket.unclebear.popgames.games.Role;
import org.bitbucket.unclebear.popgames.utils.Dimension;

class Ball extends Role {
    private boolean isDropped;
    private boolean isMovingRight = true;
    private boolean isMovingDown = true;

    Ball(int x, int y) {
        super(x, y);
        setBlock(new boolean[][]{{true}});
    }

    void moveOneStep(Brick brick, Tablet tablet) {
        adjustOrientation(brick, tablet);
        setX(getX() + (isMovingRight ? 1 : -1));
        setY(getY() + (isMovingDown ? 1 : -1));
    }

    boolean isDropped() {
        return isDropped;
    }

    private void adjustOrientation(Brick brick, Tablet tablet) {
        int nextX = getX() + (isMovingRight ? 1 : -1);
        int nextY = getY() + (isMovingDown ? 1 : -1);
        if (getY() > Dimension.PIXELS_AMOUNT_Y) {
            isDropped = true;
        } else {
            adjustInRange(brick, nextX, nextY);
        }
        if (getY() == Dimension.PIXELS_AMOUNT_Y - 2) {
            adjustOnMeetingTablet(tablet);
        }
    }

    private void adjustInRange(Brick brick, int nextX, int nextY) {
        if (nextX < 0 || nextX >= Dimension.PIXELS_AMOUNT_X || nextY < 0) {
            adjustOnMeetingBorder(nextX, nextY);
        } else if (brick.isInBlockAndUp(nextX, nextY)) {
            adjustOnMeetingBrick(brick, nextX, nextY);
        } else if (brick.isInBlockAndUp(nextX, getY()) || brick.isInBlockAndUp(getX(), nextY)) {
            adjustOnMeetingBrickAside(brick, nextX, nextY);
        }
    }

    private void adjustOnMeetingBorder(int nextX, int nextY) {
        Sound.play(R.raw.effect_negative);
        if (nextX < 0 || nextX >= Dimension.PIXELS_AMOUNT_X) {
            isMovingRight = !isMovingRight;
        }
        if (nextY < 0) {
            isMovingDown = !isMovingDown;
        }
    }

    private void adjustOnMeetingBrick(Brick brick, int nextX, int nextY) {
        Sound.play(R.raw.effect_positive);
        isMovingRight = !isMovingRight;
        isMovingDown = !isMovingDown;
        brick.remove(nextX, nextY);
        brick.remove(nextX, getY());
        brick.remove(getX(), nextY);
    }

    private void adjustOnMeetingBrickAside(Brick brick, int nextX, int nextY) {
        Sound.play(R.raw.effect_positive);
        if (brick.isInBlockAndUp(nextX, getY())) {
            isMovingRight = !isMovingRight;
            brick.remove(nextX, getY());
        } else if (brick.isInBlockAndUp(getX(), nextY)) {
            isMovingDown = !isMovingDown;
            brick.remove(getX(), nextY);
        }
    }

    private void adjustOnMeetingTablet(Tablet tablet) {
        int dropPoint = getX() - tablet.getX();
        switch (dropPoint) {
            case 0:
                adjustOnMostLeft();
                break;
            case 1:
                adjustOnLeft();
                break;
            case 2:
                adjustOnMiddle();
                break;
            case 3:
                adjustOnRight();
                break;
            case 4:
                adjustOnMostRight();
                break;
            default:
                break;
        }
    }

    private void adjustOnMostLeft() {
        if (isMovingRight) {
            Sound.play(R.raw.effect_click);
            isMovingDown = !isMovingDown;
            isMovingRight = !isMovingRight;
        }
    }

    private void adjustOnLeft() {
        Sound.play(R.raw.effect_click);
        isMovingDown = !isMovingDown;
        setX(getX() - 1);
    }

    private void adjustOnMiddle() {
        Sound.play(R.raw.effect_click);
        isMovingDown = !isMovingDown;
    }

    private void adjustOnRight() {
        Sound.play(R.raw.effect_click);
        isMovingDown = !isMovingDown;
        setX(getX() + 1);
    }

    private void adjustOnMostRight() {
        if (!isMovingRight) {
            Sound.play(R.raw.effect_click);
            isMovingDown = !isMovingDown;
            isMovingRight = !isMovingRight;
        }
    }
}

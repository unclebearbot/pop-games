package org.bitbucket.unclebear.popgames.beans;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.utils.Resource;
import org.bitbucket.unclebear.popgames.utils.Website;

public class About {
    private About() {
    }

    public static void show() {
        String message = Resource.getString(R.string.about_brief) + "\n" + Resource.getString(R.string.about_copyright);
        String negativeButton = Resource.getString(R.string.close);
        String positiveButton = Resource.getString(R.string.visit_the_website);
        Popup.show(message, negativeButton, positiveButton, (dialogInterface, i) -> viewWebsite());
    }

    private static void viewWebsite() {
        Website.view(Resource.getString(R.string.website_url));
    }
}

package org.bitbucket.unclebear.popgames.beans;

import android.app.AlertDialog;
import android.content.DialogInterface;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.utils.Resource;

public class Popup {
    private static boolean isShowing;

    private Popup() {
    }

    public static void show(String message, String negativeButton, String positiveButton, DialogInterface.OnClickListener positiveListener) {
        if (isShowing) {
            return;
        }
        isShowing = true;
        Resource.getHandler().post(
                new AlertDialog.Builder(Resource.getContext())
                        .setTitle(Resource.getString(R.string.app_name))
                        .setMessage(message)
                        .setNegativeButton(negativeButton, (dialogInterface, i) -> hide())
                        .setPositiveButton(
                                positiveButton, (dialogInterface, i) -> {
                                    positiveListener.onClick(dialogInterface, i);
                                    hide();
                                }
                        )
                        .setOnCancelListener(dialogInterface -> hide())
                        ::show
        );
    }

    private static void hide() {
        isShowing = false;
    }
}

package org.bitbucket.unclebear.popgames.controllers;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.utils.Resource;

import java.util.HashMap;
import java.util.Map;

public class Alphabet {
    private static Map<Character, boolean[][]> rects;

    static {
        inflateRects();
    }

    private Alphabet() {
    }

    public static boolean[][] getRect(char c) {
        boolean isNumber = '0' <= c && c <= '9';
        boolean isUppercase = 'A' <= c && c <= 'Z';
        boolean isLowercase = 'a' <= c && c <= 'z';
        if (isNumber || isUppercase || isLowercase) {
            return rects.get(c);
        }
        return new boolean[0][0];
    }

    private static void inflateRects() {
        rects = new HashMap<>();
        int id = R.string.character_0;
        for (char c = '0'; c <= '9'; ++c) {
            rects.put(c, decodeRect(Resource.getString(id++)));
        }
        id = R.string.character_a;
        for (char c = 'A'; c <= 'Z'; ++c) {
            rects.put(c, decodeRect(Resource.getString(id++)));
        }
        id = R.string.thumbnail_a;
        for (char c = 'a'; c <= 'z'; ++c) {
            rects.put(c, decodeRect(Resource.getString(id++)));
            if (id > R.string.thumbnail_e) {
                id = R.string.thumbnail_e;
            }
        }
    }

    public static boolean[][] decodeRect(String string) {
        String[] line = string.split(" ");
        int width = line[0].length();
        int height = line.length;
        boolean[][] rect = new boolean[width][height];
        for (int j = 0; j < height; ++j) {
            char[] chars = line[j].toCharArray();
            for (int i = 0; i < width; ++i) {
                rect[i][j] = chars[i] == '#';
            }
        }
        return rect;
    }
}

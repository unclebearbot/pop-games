package org.bitbucket.unclebear.popgames.games.snake;

import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.games.Role;
import org.bitbucket.unclebear.popgames.utils.Dimension;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Snake extends Role {
    private boolean isAlive = true;
    private LinkedList<int[]> body;
    private ArrayList<int[]> tailsToClear;
    private Button.Key orientation;
    private Button.Key nextOrientation;

    Snake(int x, int y) {
        super(x, y);
        int centerX = Dimension.PIXELS_AMOUNT_X / 2;
        int centerY = Dimension.PIXELS_AMOUNT_Y / 2;
        body = new LinkedList<>();
        body.addFirst(new int[]{centerX, centerY + 3});
        body.addFirst(new int[]{centerX, centerY + 2});
        body.addFirst(new int[]{centerX, centerY + 1});
        tailsToClear = new ArrayList<>();
        orientation = Button.Key.UP;
        nextOrientation = Button.Key.UP;
    }

    boolean isAlive() {
        return isAlive;
    }

    private void setAlive(boolean alive) {
        isAlive = alive;
    }

    Button.Key getOrientation() {
        return orientation;
    }

    void setOrientationByKey(Button.Key key) {
        switch (key) {
            case UP:
                adjustOrientationOnKeyUp();
                break;
            case DOWN:
                adjustOrientationOnKeyDown();
                break;
            case LEFT:
                adjustOrientationOnKeyLeft();
                break;
            case RIGHT:
                adjustOrientationOnKeyRight();
                break;
            default:
                break;
        }
    }

    private void adjustOrientationOnKeyUp() {
        if (orientation != Button.Key.DOWN) {
            nextOrientation = Button.Key.UP;
        }
    }

    private void adjustOrientationOnKeyDown() {
        if (orientation != Button.Key.UP) {
            nextOrientation = Button.Key.DOWN;
        }
    }

    private void adjustOrientationOnKeyLeft() {
        if (orientation != Button.Key.RIGHT) {
            nextOrientation = Button.Key.LEFT;
        }
    }

    private void adjustOrientationOnKeyRight() {
        if (orientation != Button.Key.LEFT) {
            nextOrientation = Button.Key.RIGHT;
        }
    }

    void moveOneStep(Apple apple) {
        orientation = nextOrientation;
        addHead();
        if (willDie()) {
            setAlive(false);
            return;
        }
        if (willEat(apple)) {
            apple.setAlive(false);
        } else {
            addTailsToClear(body.getLast());
            body.removeLast();
        }
    }

    private boolean willDie() {
        int[] head = body.getFirst();
        if (Screen.isOutOfBound(head[0], head[1])) {
            return true;
        }
        for (int i = 1; i < body.size(); ++i) {
            int[] part = body.get(i);
            if (head[0] == part[0] && head[1] == part[1]) {
                return true;
            }
        }
        return false;
    }

    private boolean willEat(Apple apple) {
        int[] head = body.getFirst();
        return apple.isInBlockAndUp(head[0], head[1]);
    }

    private void addHead() {
        int[] head = body.getFirst();
        int newX = head[0];
        int newY = head[1];
        switch (orientation) {
            case UP:
                newY = head[1] - 1;
                break;
            case DOWN:
                newY = head[1] + 1;
                break;
            case LEFT:
                newX = head[0] - 1;
                break;
            case RIGHT:
                newX = head[0] + 1;
                break;
            default:
                break;
        }
        body.addFirst(new int[]{newX, newY});
    }

    int[][] getBody() {
        return toArray(body);
    }

    private void addTailsToClear(int[] tail) {
        tailsToClear.add(tail);
    }

    void clearTails() {
        tailsToClear.clear();
    }

    int[][] getTailsToClear() {
        return toArray(tailsToClear);
    }

    private int[][] toArray(List origin) {
        Object[] objects = origin.toArray();
        int[][] result = new int[objects.length][2];
        for (int i = 0; i < result.length; ++i) {
            for (int j = 0; j < result[i].length; ++j) {
                result[i][j] = ((int[]) objects[i])[j];
            }
        }
        return result;
    }

    @Override
    public boolean isInBlockAndUp(int x, int y) {
        for (int[] part : toArray(body)) {
            if (part[0] == x && part[1] == y) {
                return true;
            }
        }
        return false;
    }
}

package org.bitbucket.unclebear.popgames.games.snake;

import org.bitbucket.unclebear.popgames.games.Role;

class Apple extends Role {
    private boolean isAlive;

    Apple(int x, int y) {
        super(x, y);
        setBlock(new boolean[][]{{true}});
    }

    boolean isAlive() {
        return isAlive;
    }

    void setAlive(boolean alive) {
        isAlive = alive;
    }

    @Override
    public boolean isInBlockAndUp(int x, int y) {
        return getX() == x && getY() == y;
    }
}

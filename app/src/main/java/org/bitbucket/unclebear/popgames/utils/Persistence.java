package org.bitbucket.unclebear.popgames.utils;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Persistence {
    private static Context context;

    static {
        context = Resource.getContext();
    }

    private Persistence() {
    }

    public static void serialize(String file, Object object) {
        if (context == null) {
            return;
        }
        new Thread(() -> {
            context.deleteFile(file);
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(context.openFileOutput(file, Context.MODE_PRIVATE))) {
                objectOutputStream.writeObject(object);
            } catch (IOException e) {
                Log.e(Dimension.ERROR, e.getMessage(), e);
            }
        }).start();
    }

    public static <V> V deserialize(String file, V fallback) {
        if (context == null) {
            return fallback;
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(context.openFileInput(file))) {
            return new Caster<V>().cast(objectInputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            Log.e(Dimension.ERROR, e.getMessage(), e);
        }
        return fallback;
    }
}

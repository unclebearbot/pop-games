package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;

public class MyInteger {
    private MyNumber[] myNumbers;
    private int limit;
    private int value;
    private int width;

    public MyInteger(int digit, int x, int y, int size) {
        int tempDigit = digit;
        tempDigit = tempDigit < 1 ? 1 : tempDigit;
        limit = 1;
        for (int i = 0; i < tempDigit; ++i) {
            limit *= 10;
        }
        limit -= 1;
        final float spacingFactor = 0.5f;
        int spacing = (int) (size * spacingFactor);
        spacing = spacing < 1 ? 1 : spacing;
        width = size * tempDigit + spacing * (tempDigit - 1);
        myNumbers = new MyNumber[tempDigit];
        for (int i = 0; i < myNumbers.length; ++i) {
            myNumbers[i] = new MyNumber(x + (size + spacing) * i, y, size);
        }
    }

    public void draw(Canvas canvas) {
        for (MyNumber myNumber : myNumbers) {
            myNumber.draw(canvas);
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        int tempValue = value;
        tempValue = tempValue > limit ? limit : tempValue;
        tempValue = tempValue < 0 ? 0 : tempValue;
        this.value = tempValue;
        for (int j = 1; j < myNumbers.length + 1; ++j) {
            int number = tempValue % 10;
            myNumbers[myNumbers.length - j].setValue(number);
            tempValue /= 10;
        }
    }

    public int getWidth() {
        return width;
    }
}

package org.bitbucket.unclebear.popgames.games.brick;

import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.games.Role;
import org.bitbucket.unclebear.popgames.utils.Dimension;

class Brick extends Role {
    private Game game;

    Brick(Game game, int x, int y) {
        super(x, y);
        this.game = game;
    }

    boolean isCleared() {
        for (boolean[] line : getBlock()) {
            for (boolean dot : line) {
                if (dot) {
                    return false;
                }
            }
        }
        return true;
    }

    void fill(int depth) {
        boolean[][] block = new boolean[Dimension.PIXELS_AMOUNT_X][depth];
        for (int i = 2; i < block.length - 2; ++i) {
            for (int j = 2; j < block[i].length; ++j) {
                block[i][j] = true;
            }
        }
        setBlock(block);
    }

    @Override
    public void remove(int x, int y) {
        boolean isInRange = 0 <= x && x < getBlock().length && 0 <= y && y < getBlock()[x].length;
        if (isInRange && getBlock()[x][y]) {
            getBlock()[x][y] = false;
            game.increaseScore(10);
        }
    }
}

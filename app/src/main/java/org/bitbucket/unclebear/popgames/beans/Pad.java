package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.List;

public class Pad {
    private static Button[] buttons;
    private static ArrayList<Button.Key> keys = new ArrayList<>();

    private Pad() {
    }

    public static void initialize(int x, int y, int width) {
        if (buttons != null) {
            return;
        }
        final float buttonSizeFactor = 0.125f;
        int buttonSize = (int) (width * buttonSizeFactor);
        Button buttonUp = new Button(Button.Key.UP, x + buttonSize, y + buttonSize / 2, buttonSize);
        Button buttonDown = new Button(Button.Key.DOWN, x + buttonSize, (int) (y + buttonSize * 2.5f), buttonSize);
        Button buttonLeft = new Button(Button.Key.LEFT, x, (int) (y + buttonSize * 1.5f), buttonSize);
        Button buttonRight = new Button(Button.Key.RIGHT, x + buttonSize * 2, (int) (y + buttonSize * 1.5f), buttonSize);
        Button buttonAb = new Button(Button.Key.AB, x + buttonSize * 6, y + buttonSize, buttonSize * 2);
        Button buttonMenu = new Button(Button.Key.MENU, (int) (x + buttonSize * 2.75f), (int) (y + buttonSize * 3.5f - buttonSize * 0.5f), buttonSize);
        Button buttonStart = new Button(Button.Key.START, (int) (x + buttonSize * 4.25f), (int) (y + buttonSize * 3.5f - buttonSize * 0.5f), buttonSize);
        buttons = new Button[]{buttonUp, buttonDown, buttonLeft, buttonRight, buttonAb, buttonMenu, buttonStart};
    }

    public static List<Button.Key> getPressedButtonsKeys() {
        if (buttons == null) {
            return new ArrayList<>();
        }
        keys.clear();
        for (Button button : buttons) {
            if (button.isPressed()) {
                keys.add(button.getKey());
            }
        }
        return keys;
    }

    public static void draw(Canvas canvas) {
        if (buttons == null) {
            return;
        }
        for (Button button : buttons) {
            button.draw(canvas);
        }
    }

    public static void touch(MotionEvent motionEvent) {
        if (buttons == null) {
            return;
        }
        int action = motionEvent.getAction();
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                onTouchMove(motionEvent);
                break;
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN | 0x0100:
                onTouchDown(motionEvent);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                onTouchUpFirst(motionEvent);
                break;
            case MotionEvent.ACTION_POINTER_UP | 0x0100:
                onTouchUpSecond(motionEvent);
                break;
            default:
                break;
        }
    }

    private static void onTouchMove(MotionEvent motionEvent) {
        for (Button button : buttons) {
            processMove(button, motionEvent);
        }
    }

    private static void processMove(Button button, MotionEvent motionEvent) {
        int count = motionEvent.getPointerCount();
        for (int i = 0; i < count; ++i) {
            if (button.isInArea((int) motionEvent.getX(i), (int) motionEvent.getY(i))) {
                button.setPressed(true);
                break;
            }
            button.setPressed(false);
        }
    }

    private static void onTouchDown(MotionEvent motionEvent) {
        for (Button button : buttons) {
            processDown(button, motionEvent);
        }
    }

    private static void processDown(Button button, MotionEvent motionEvent) {
        int count = motionEvent.getPointerCount();
        for (int i = 0; i < count; ++i) {
            if (!button.isPressed() && button.isInArea((int) motionEvent.getX(i), (int) motionEvent.getY(i))) {
                button.setPressed(true);
                break;
            }
        }
    }

    private static void onTouchUpFirst(MotionEvent motionEvent) {
        for (Button button : buttons) {
            if (button.isPressed() && button.isInArea((int) motionEvent.getX(), (int) motionEvent.getY())) {
                button.setPressed(false);
            }
        }
    }

    private static void onTouchUpSecond(MotionEvent motionEvent) {
        for (Button button : buttons) {
            if (button.isPressed() && button.isInArea((int) motionEvent.getX(1), (int) motionEvent.getY(1))) {
                button.setPressed(false);
            }
        }
    }
}

package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.view.MotionEvent;

import org.bitbucket.unclebear.popgames.controllers.MyVibrator;
import org.bitbucket.unclebear.popgames.controllers.Sound;

public class Console {
    private Console() {
    }

    public static void initialize(int x, int y, int width, int height) {
        final float screenHeightFactor = 0.675f;
        final float padHeightFactor = screenHeightFactor * 0.425f;
        float screenHeight = height * screenHeightFactor;
        float padHeight = height * padHeightFactor;
        final float widthHeightRatio = 0.75f;
        float screenWidth = screenHeight * widthHeightRatio;
        float padWidth = screenWidth * 1;
        float screenX = (width - screenWidth) / 2;
        float padX = (width - padWidth) / 2;
        float screenY = height - padHeight - screenHeight;
        float padY = height - padHeight;
        Body.initialize(x, y, width, height);
        Pad.initialize((int) padX, (int) padY, (int) padWidth);
        Screen.initialize((int) screenX, (int) screenY, (int) screenWidth, (int) screenHeight);
        Menu.initialize((int) screenX, (int) screenY, (int) screenWidth, (int) screenHeight);
    }

    public static void touch(MotionEvent motionEvent) {
        Pad.touch(motionEvent);
    }
    public static void draw(Canvas canvas) {
        Body.draw(canvas);
        Pad.draw(canvas);
        Screen.draw(canvas);
        Menu.draw(canvas);
    }

    public static void terminate() {
        MyVibrator.cancel();
        Sound.terminate();
    }
}

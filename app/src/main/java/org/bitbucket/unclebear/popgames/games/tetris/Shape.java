package org.bitbucket.unclebear.popgames.games.tetris;

import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.games.Role;
import org.bitbucket.unclebear.popgames.utils.Dimension;

import java.util.ArrayList;

public abstract class Shape extends Role {
    public Shape(int x, int y) {
        super(x, y);
    }

    boolean move(Button.Key orientation) {
        boolean willCollide = willCollide(orientation);
        if (willCollide) {
            return false;
        }
        boolean result = false;
        switch (orientation) {
            case DOWN:
                setY(getY() + 1);
                result = true;
                break;
            case LEFT:
                setX(getX() - 1);
                result = true;
                break;
            case RIGHT:
                setX(getX() + 1);
                result = true;
                break;
            case UP:
                boolean[][] rotatedBlock = rotate(getBlock());
                setBlock(rotatedBlock);
                result = true;
                break;
            default:
                break;
        }
        return result;
    }

    private boolean willCollide(Button.Key orientation) {
        boolean result = false;
        switch (orientation) {
            case DOWN:
                result = willCollideDown();
                break;
            case LEFT:
                result = willCollideLeft();
                break;
            case RIGHT:
                result = willCollideRight();
                break;
            case UP:
            case AB:
                result = willCollideRotating();
                break;
            default:
                break;
        }
        return result;
    }

    private boolean willCollideDown() {
        int x = getX();
        int y = getY() + 1;
        boolean isOutOfBound = y >= Dimension.PIXELS_AMOUNT_Y;
        boolean willCollide = willBlockCollideAt(x, y, getBlock());
        return isOutOfBound || willCollide;
    }

    private boolean willCollideLeft() {
        int x = getX() - 1;
        int y = getY();
        boolean isOutOfBound = x < 0;
        boolean willCollide = willBlockCollideAt(x, y, getBlock());
        return isOutOfBound || willCollide;
    }

    private boolean willCollideRight() {
        int x = getX() + 1;
        int y = getY();
        boolean isOutOfBound = x >= Dimension.PIXELS_AMOUNT_X;
        boolean willCollide = willBlockCollideAt(x, y, getBlock());
        return isOutOfBound || willCollide;
    }

    private boolean willCollideRotating() {
        int x = getX();
        int y = getY();
        return willBlockCollideAt(x, y, rotate(getBlock()));
    }

    boolean willBlockCollideAt(int x, int y, boolean[][] block) {
        ArrayList<int[]> toCheck = new ArrayList<>();
        for (int i = 0; i < block.length; ++i) {
            for (int j = 0; j < block[i].length; ++j) {
                if (Screen.isOutOfBound(x + i, y + j)) {
                    return true;
                }
                if (block[i][j]) {
                    toCheck.add(new int[]{x + i, y + j});
                }
            }
        }
        Object[] objects = toCheck.toArray();
        int[][] result = new int[objects.length][2];
        for (int i = 0; i < result.length; ++i) {
            for (int j = 0; j < result[i].length; ++j) {
                result[i][j] = ((int[]) objects[i])[j];
            }
        }
        return Screen.isAnyUp(result);
    }

    boolean[][] rotate(boolean[][] block) {
        boolean[][] result = new boolean[block[0].length][block.length];
        for (int i = 0; i < block.length; ++i) {
            int length = block[i].length;
            for (int j = 0; j < length; ++j) {
                result[length - j - 1][i] = block[i][j];
            }
        }
        return result;
    }
}

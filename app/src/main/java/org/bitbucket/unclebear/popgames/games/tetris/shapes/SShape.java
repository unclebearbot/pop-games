package org.bitbucket.unclebear.popgames.games.tetris.shapes;

import org.bitbucket.unclebear.popgames.games.tetris.Shape;

public class SShape extends Shape {
    public SShape(int x, int y) {
        super(x, y);
        boolean[][] block = new boolean[][]{{false, true}, {true, true}, {true, false}};
        setBlock(block);
    }
}

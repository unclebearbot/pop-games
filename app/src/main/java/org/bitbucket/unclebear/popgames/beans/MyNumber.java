package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;

public class MyNumber {
    private Stick[] sticks;

    public MyNumber(int x, int y, int size) {
        sticks = new Stick[7];
        int[][] vertexes = new int[5][2];
        for (int i = 0; i < vertexes.length; ++i) {
            vertexes[i][0] = x + size * (i % 2);
            vertexes[i][1] = y + size * (i / 2);
        }
        sticks[0] = new Stick(true, vertexes[0][0], vertexes[0][1], size);
        sticks[1] = new Stick(true, vertexes[2][0], vertexes[2][1], size);
        sticks[2] = new Stick(true, vertexes[4][0], vertexes[4][1], size);
        sticks[3] = new Stick(false, vertexes[0][0], vertexes[0][1], size);
        sticks[4] = new Stick(false, vertexes[1][0], vertexes[1][1], size);
        sticks[5] = new Stick(false, vertexes[2][0], vertexes[2][1], size);
        sticks[6] = new Stick(false, vertexes[3][0], vertexes[3][1], size);
    }

    public void draw(Canvas canvas) {
        for (Stick stick : sticks) {
            stick.draw(canvas);
        }
    }

    public void setValue(int value) {
        switch (value) {
            case 1:
                lightUpSticks(new boolean[]{false, false, false, false, true, false, true});
                break;
            case 2:
                lightUpSticks(new boolean[]{true, true, true, false, true, true, false});
                break;
            case 3:
                lightUpSticks(new boolean[]{true, true, true, false, true, false, true});
                break;
            case 4:
                lightUpSticks(new boolean[]{false, true, false, true, true, false, true});
                break;
            case 5:
                lightUpSticks(new boolean[]{true, true, true, true, false, false, true});
                break;
            case 6:
                lightUpSticks(new boolean[]{true, true, true, true, false, true, true});
                break;
            case 7:
                lightUpSticks(new boolean[]{true, false, false, true, true, false, true});
                break;
            case 8:
                lightUpSticks(new boolean[]{true, true, true, true, true, true, true});
                break;
            case 9:
                lightUpSticks(new boolean[]{true, true, true, true, true, false, true});
                break;
            default:
                lightUpSticks(new boolean[]{true, false, true, true, true, true, true});
                break;
        }
    }

    private void lightUpSticks(boolean[] sticks) {
        if (sticks.length == 7) {
            for (int i = 0; i < sticks.length; ++i) {
                this.sticks[i].lightUp(sticks[i]);
            }
        }
    }
}

package org.bitbucket.unclebear.popgames.games.racing;

import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.games.Role;
import org.bitbucket.unclebear.popgames.utils.Dimension;

class Car extends Role {
    private boolean[][] block = new boolean[][]{{false, true, false, true}, {true, true, true, false}, {false, true, false, true}};
    private boolean isHero;
    private boolean heroHeartBeat;
    private boolean isAlive = true;

    Car(boolean hero, int x, int y) {
        super(x, y);
        isHero = hero;
        setBlock(block);
    }

    boolean isAlive() {
        return isAlive;
    }

    void setAlive(boolean alive) {
        isAlive = alive;
    }

    void heroBeat() {
        if (isHero) {
            block[1][1] = heroHeartBeat;
            heroHeartBeat = !heroHeartBeat;
        }
    }

    void enemyMove(Game game) {
        if (!isHero) {
            if (getY() >= Dimension.PIXELS_AMOUNT_Y) {
                setAlive(false);
                game.increaseScore(10);
                return;
            }
            setY(getY() + 1);
        }
    }

    boolean isEnemyCollided(Car hero) {
        if (getY() < hero.getY()) {
            return !isHero && getX() == hero.getX() && getY() + 3 > hero.getY();
        } else return !isHero && getX() == hero.getX() && hero.getY() + 3 > getY();
    }
}

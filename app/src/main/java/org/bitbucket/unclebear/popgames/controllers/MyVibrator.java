package org.bitbucket.unclebear.popgames.controllers;

import android.content.Context;
import android.os.Vibrator;

import org.bitbucket.unclebear.popgames.utils.Resource;
import org.bitbucket.unclebear.popgames.utils.Setting;

public class MyVibrator {
    public static final int MAX_DURATION = 40;
    private static int duration = MAX_DURATION;
    private static Vibrator vibrator;

    static {
        vibrator = (Vibrator) Resource.getContext().getSystemService(Context.VIBRATOR_SERVICE);
        duration = Setting.get(Setting.VIBRATION, MAX_DURATION);
    }

    private MyVibrator() {
    }

    public static void vibrate() {
        if (vibrator == null || duration == 0 || !vibrator.hasVibrator()) {
            return;
        }
        vibrator.vibrate(duration);
    }

    public static void cancel() {
        if (vibrator == null) {
            return;
        }
        vibrator.cancel();
    }

    public static void setDuration(int duration) {
        MyVibrator.duration = duration;
        Setting.set(Setting.VIBRATION, duration);
    }
}

package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.controllers.MyVibrator;
import org.bitbucket.unclebear.popgames.controllers.Sound;
import org.bitbucket.unclebear.popgames.controllers.Stage;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Resource;
import org.bitbucket.unclebear.popgames.utils.Setting;

import java.util.ArrayList;

public class Menu {
    private static final int LIST_MAX_LENGTH = 10;
    private static final float ENTRY_TEXT_SIZE = 0.5f;
    private static int index;
    private static ArrayList<Entry> entries;
    private static boolean isStarted;
    private static int listX;
    private static int listY;
    private static int entryWidth;
    private static int entryHeight;
    private static Paint entryKeyPaint;
    private static Paint entryKeyHighlightedPaint;
    private static Paint entryValuePaint;
    private static Paint entryValueHighlightedPaint;
    private static Paint entryBackgroundHighlightedPaint;

    private Menu() {
    }

    public static void initialize(int screenX, int screenY, int screenWidth, int screenHeight) {
        if (entries != null) {
            return;
        }
        entries = new ArrayList<>();
        final float listWidth = 0.8f;
        final float listHeight = 0.9f;
        final float entryHeightFactor = listHeight / LIST_MAX_LENGTH;
        listX = (int) (screenX + screenWidth * (1 - listWidth) / 2);
        listY = (int) (screenY + screenHeight * (1 - listHeight) / 2);
        entryWidth = (int) (screenWidth * listWidth);
        entryHeight = (int) (screenHeight * entryHeightFactor);
        entryKeyPaint = new Paint();
        entryKeyPaint.setAntiAlias(true);
        entryKeyPaint.setColor(Color.BLACK);
        entryKeyPaint.setAlpha(Dimension.LCD_ON_ALPHA);
        entryKeyPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        entryKeyPaint.setTextAlign(Paint.Align.LEFT);
        entryKeyPaint.setTextSize(entryHeight * ENTRY_TEXT_SIZE);
        entryKeyPaint.setTypeface(Typeface.SANS_SERIF);
        entryKeyHighlightedPaint = new Paint();
        entryKeyHighlightedPaint.set(entryKeyPaint);
        entryKeyHighlightedPaint.setColor(Color.LTGRAY);
        entryKeyHighlightedPaint.setAlpha(255);
        entryValuePaint = new Paint();
        entryValuePaint.set(entryKeyPaint);
        entryValuePaint.setTextAlign(Paint.Align.RIGHT);
        entryValueHighlightedPaint = new Paint();
        entryValueHighlightedPaint.set(entryValuePaint);
        entryValueHighlightedPaint.setColor(Color.LTGRAY);
        entryValueHighlightedPaint.setAlpha(255);
        entryBackgroundHighlightedPaint = new Paint();
        entryBackgroundHighlightedPaint.set(entryKeyPaint);
        inflateEntries();
    }

    public static void pressButton(Button.Key key) {
        if (entries == null) {
            return;
        }
        switch (key) {
            case UP:
                decreaseIndex();
                break;
            case DOWN:
                increaseIndex();
                break;
            case LEFT:
                changeEntryBackward();
                break;
            case RIGHT:
            case AB:
                changeEntryForward();
                break;
            default:
                break;
        }
    }

    public static void start() {
        if (entries == null) {
            return;
        }
        isStarted = true;
        index = 0;
        Resource.inflateBitmapPool(Body.getSkinIndex());
        Screen.setShouldShowContent(false);
    }

    public static void terminate() {
        if (entries == null) {
            return;
        }
        isStarted = false;
        Resource.deflateBitmapPool();
        Screen.setShouldShowContent(true);
    }

    public static void draw(Canvas canvas) {
        if (entries == null || !isStarted) {
            return;
        }
        final float entryTextSpacingFactor = 0.05f;
        int entryTextSpacing = (int) (entryWidth * entryTextSpacingFactor);
        for (int i = 0; i < LIST_MAX_LENGTH && i < entries.size(); ++i) {
            if (i == index) {
                canvas.drawRect(listX, listY + (float) entryHeight * i, listX + (float) entryWidth, listY + (float) entryHeight * (i + 1), entryBackgroundHighlightedPaint);
            }
            canvas.drawText(entries.get(i).getKey(), listX + (float) entryTextSpacing, listY + (float) entryHeight * (i + 0.5f + ENTRY_TEXT_SIZE / 2), i == index ? entryKeyHighlightedPaint : entryKeyPaint);
            canvas.drawText(entries.get(i).getValueName(), listX + (float) entryWidth - entryTextSpacing, listY + (float) entryHeight * (i + 0.5f + ENTRY_TEXT_SIZE / 2), i == index ? entryValueHighlightedPaint : entryValuePaint);
        }
    }

    private static void increaseIndex() {
        Sound.play(R.raw.effect_click);
        ++index;
        if (index == LIST_MAX_LENGTH) {
            index = 0;
        }
        if (index == entries.size()) {
            index = 0;
        }
    }

    private static void decreaseIndex() {
        Sound.play(R.raw.effect_click);
        --index;
        if (index == -1) {
            index = LIST_MAX_LENGTH - 1;
        }
        if (index >= entries.size()) {
            index = entries.size() - 1;
        }
    }

    private static void changeEntryForward() {
        Sound.play(R.raw.effect_click);
        entries.get(index).change(true);
    }

    private static void changeEntryBackward() {
        Sound.play(R.raw.effect_click);
        entries.get(index).change(false);
    }

    private static void inflateEntries() {
        String[] enterSymbols = new String[]{Resource.getString(R.string.enter_symbol)};
        Number[] zeros = new Number[]{0};
        String[] volumes = new String[]{Resource.getString(R.string.high), Resource.getString(R.string.medium), Resource.getString(R.string.low), Resource.getString(R.string.off)};
        Number[] volumeValues = new Number[]{Sound.MAX_VOLUME, Sound.MAX_VOLUME * 0.5f, Sound.MAX_VOLUME * 0.2f, 0f};
        float music = Setting.get(Setting.MUSIC, Sound.MAX_VOLUME);
        String[] musicVolumes = volumes.clone();
        Number[] musicVolumeValues = volumeValues.clone();
        for (int i = 0; i < volumeValues.length; ++i) {
            if (volumeValues[i].equals(music)) {
                musicVolumes = reorderByIndex(musicVolumes, i);
                musicVolumeValues = reorderByIndex(musicVolumeValues, i);
                break;
            }
        }
        Entry musicEntry = new Entry(Resource.getString(R.string.music), musicVolumes, musicVolumeValues);
        musicEntry.setFunction(() -> Sound.setMusicVolume((Float) musicEntry.getValue()));
        entries.add(musicEntry);
        float effect = Setting.get(Setting.EFFECT, Sound.MAX_VOLUME);
        String[] effectVolumes = volumes.clone();
        Number[] effectVolumeValues = volumeValues.clone();
        for (int i = 0; i < volumeValues.length; ++i) {
            if (volumeValues[i].equals(effect)) {
                effectVolumes = reorderByIndex(effectVolumes, i);
                effectVolumeValues = reorderByIndex(effectVolumeValues, i);
                break;
            }
        }
        Entry effectEntry = new Entry(Resource.getString(R.string.effect), effectVolumes, effectVolumeValues);
        effectEntry.setFunction(() -> Sound.setEffectVolume((Float) effectEntry.getValue()));
        entries.add(effectEntry);
        String[] vibrationStrengths = new String[]{Resource.getString(R.string.strong), Resource.getString(R.string.medium), Resource.getString(R.string.weak), Resource.getString(R.string.off)};
        Number[] vibrationValues = new Number[]{MyVibrator.MAX_DURATION, MyVibrator.MAX_DURATION / 2, MyVibrator.MAX_DURATION / 4, 0};
        int vibration = Setting.get(Setting.VIBRATION, MyVibrator.MAX_DURATION);
        for (int i = 0; i < vibrationValues.length; ++i) {
            if (vibrationValues[i].equals(vibration)) {
                vibrationStrengths = reorderByIndex(vibrationStrengths, i);
                vibrationValues = reorderByIndex(vibrationValues, i);
                break;
            }
        }
        Entry vibrationEntry = new Entry(Resource.getString(R.string.vibration), vibrationStrengths, vibrationValues);
        vibrationEntry.setFunction(() -> MyVibrator.setDuration(vibrationEntry.getValue().intValue()));
        entries.add(vibrationEntry);
        int skinIndex = Body.getSkinIndex();
        String[] skinNames = reorderByIndex(Body.getSkinNames(), skinIndex);
        Number[] skins = new Number[skinNames.length];
        for (int i = 0; i < skins.length; ++i) {
            skins[i] = i;
        }
        skins = reorderByIndex(skins, skinIndex);
        Entry skinEntry = new Entry(Resource.getString(R.string.skin), skinNames, skins);
        skinEntry.setFunction(() -> {
            int index = skinEntry.getValue().intValue();
            int obsolete = Body.getSkinIndex();
            Body.setSkinIndex(index);
            Resource.deflateBitmapPoolAround(obsolete);
            Resource.inflateBitmapPool(index);
        });
        entries.add(skinEntry);
        Entry reinforcementEntry = new Entry(Resource.getString(R.string.reinforcement), enterSymbols, zeros);
        reinforcementEntry.setFunction(Reinforcement::show);
        entries.add(reinforcementEntry);
        Entry aboutEntry = new Entry(Resource.getString(R.string.about), enterSymbols, zeros);
        aboutEntry.setFunction(About::show);
        entries.add(aboutEntry);
        Entry resetEntry = new Entry(Resource.getString(R.string.reset), enterSymbols, zeros);
        resetEntry.setFunction(Stage::reset);
        entries.add(resetEntry);
    }

    private static <T> T[] reorderByIndex(T[] origin, int index) {
        int tempIndex = index;
        int length = origin.length;
        if (tempIndex <= 0 || tempIndex >= length) {
            return origin;
        }
        T[] result = origin.clone();
        for (int i = 0; i < length; ++i) {
            result[i] = origin[tempIndex++];
            tempIndex = tempIndex >= length ? 0 : tempIndex;
        }
        return result;
    }
}

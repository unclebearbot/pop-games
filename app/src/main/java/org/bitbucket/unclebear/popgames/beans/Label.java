package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import org.bitbucket.unclebear.popgames.utils.Dimension;

public class Label {
    private String content;
    private Paint paint;
    private int x;
    private int y;

    public Label(String content, int x, int y, int size) {
        this.content = content;
        this.x = x;
        this.y = y;
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setAlpha(Dimension.LCD_ON_ALPHA);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setTextAlign(Paint.Align.RIGHT);
        paint.setTextSize(size);
        paint.setTypeface(Typeface.MONOSPACE);
    }

    public void draw(Canvas canvas) {
        canvas.drawText(content, x, y, paint);
    }

    public void lightUp(boolean willLightUp) {
        paint.setAlpha(willLightUp ? Dimension.LCD_ON_ALPHA : Dimension.LCD_OFF_ALPHA);
    }
}

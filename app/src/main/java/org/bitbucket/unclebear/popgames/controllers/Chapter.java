package org.bitbucket.unclebear.popgames.controllers;

import android.util.Log;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Setting;

public class Chapter {
    private static char currentChapter = 'A';
    private static int mission = 1;
    private static boolean isFirstTime = true;

    private Chapter() {
    }

    public static void gameBeginning() {
        Sound.play(R.raw.music_game_beginning);
        Gaming.start(currentChapter);
        Hud.showHighLabel(false);
        Hud.setScore(0);
    }

    public static void gameOver() {
        updatePixels();
    }

    private static void updatePixels() {
        Screen.updatePixelsInBlock(Alphabet.getRect(currentChapter), 2, 2);
        Screen.updatePixelsInRect(true, new int[]{0, 0, 10, 1});
        Screen.updatePixelsInRect(true, new int[]{0, 8, 10, 1});
        Screen.updatePixelsInRect(true, new int[]{0, 1, 1, 7});
        Screen.updatePixelsInRect(true, new int[]{9, 1, 1, 7});
        final int offset = 'a' - 'A';
        Screen.updatePixelsInBlock(Alphabet.getRect((char) (currentChapter + offset)), 1, 9);
        Screen.updatePixelsInRect(true, new int[]{0, 12, 10, 1});
        Screen.updatePixelsInRect(true, new int[]{0, 9, 1, 3});
        Screen.updatePixelsInRect(true, new int[]{9, 9, 1, 3});
        Hud.showHighLabel(true);
        Hud.setScore(Setting.get(Setting.SCORE + currentChapter, 0));
        Screen.updatePixelsInBlock(Alphabet.getRect((char) ('0' + mission / 10)), 1, 14);
        Screen.updatePixelsInBlock(Alphabet.getRect((char) ('0' + mission % 10)), 6, 14);
    }

    public static void pressButton(Button.Key key) {
        switch (key) {
            case UP:
                increaseSpeed();
                break;
            case DOWN:
                increaseLevel();
                break;
            case LEFT:
                decreaseMission();
                break;
            case RIGHT:
                increaseMission();
                break;
            case AB:
                increaseChapter();
                break;
            default:
                break;
        }
        updatePixels();
    }

    public static void start() {
        if (isFirstTime) {
            isFirstTime = false;
            Sound.play(R.raw.music_welcome);
        }
        updatePixels();
    }

    public static void reset() {
        isFirstTime = true;
        currentChapter = 'A';
        mission = 1;
        Hud.setScore(888888);
        Hud.setSpeed(88);
        Hud.setLevel(88);
        Hud.showPausingIcon(true);
        Screen.updatePixelsInRect(true, new int[]{0, 0, Dimension.PIXELS_AMOUNT_X, Dimension.PIXELS_AMOUNT_Y});
        Screen.updateAuxiliaryPixelsInLine(4);
        final int resetDuration = 500;
        try {
            Thread.sleep(resetDuration);
        } catch (InterruptedException e) {
            Log.e(Dimension.ERROR, e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
        Screen.clearPixels();
        Screen.clearAuxiliaryPixels();
        Hud.setScore(0);
        Hud.setSpeed(Hud.BASE_SPEED_AND_LEVEL);
        Hud.setLevel(Hud.BASE_SPEED_AND_LEVEL);
        Hud.showPausingIcon(false);
        start();
    }

    public static char getCurrentChapter() {
        return currentChapter;
    }

    public static void setCurrentChapter(char currentChapter) {
        Chapter.currentChapter = currentChapter;
        updatePixels();
    }

    public static int getMission() {
        return mission;
    }

    public static void setMission(int mission) {
        Chapter.mission = mission;
    }

    private static void increaseChapter() {
        Sound.play(R.raw.effect_click);
        ++currentChapter;
        if (currentChapter > 'Z') {
            currentChapter = 'A';
        }
        Hud.showHighLabel(true);
        Hud.setScore(Setting.get(Setting.SCORE + currentChapter, 0));
    }

    private static void increaseMission() {
        Sound.play(R.raw.effect_click);
        ++mission;
        if (mission > 99) {
            mission = 1;
        }
    }

    private static void decreaseMission() {
        Sound.play(R.raw.effect_click);
        --mission;
        if (mission < 1) {
            mission = 99;
        }
    }

    private static void increaseSpeed() {
        Sound.play(R.raw.effect_click);
        if (Hud.getSpeed() > 9) {
            Hud.setSpeed(1);
        } else {
            Hud.setSpeed(Hud.getSpeed() + 1);
        }
    }

    private static void increaseLevel() {
        Sound.play(R.raw.effect_click);
        if (Hud.getLevel() > 9) {
            Hud.setLevel(1);
        } else {
            Hud.setLevel(Hud.getLevel() + 1);
        }
    }
}

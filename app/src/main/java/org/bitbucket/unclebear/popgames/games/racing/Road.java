package org.bitbucket.unclebear.popgames.games.racing;

import org.bitbucket.unclebear.popgames.games.Role;

class Road extends Role {
    private boolean[][][] stages = new boolean[][][]{
            {{true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false}},
            {{false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false}},
            {{false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true}},
            {{true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true}}
    };
    private int stageIndex = 0;

    Road(int x, int y) {
        super(x, y);
        setBlock(stages[stageIndex]);
    }

    void moveOneStep() {
        ++stageIndex;
        if (stageIndex >= stages.length) {
            stageIndex = 0;
        }
        setBlock(stages[stageIndex]);
    }
}

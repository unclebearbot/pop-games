package org.bitbucket.unclebear.popgames.views;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.bitbucket.unclebear.popgames.beans.Console;
import org.bitbucket.unclebear.popgames.controllers.Stage;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Resource;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

public class MySurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    private Context context;
    private SurfaceHolder surfaceHolder;
    private SleepyThread sleepyThread;

    public MySurfaceView(Context context) {
        super(context);
        this.context = context;
        surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Resource.initialize(context);
        Console.initialize(0, 0, getWidth(), getHeight());
        start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //nothing to do
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        terminate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Console.touch(motionEvent);
        return true;
    }

    public void start() {
        if (sleepyThread != null && sleepyThread.isRunning()) {
            return;
        }
        final long drawInterval = Dimension.SECOND / Dimension.UPDATE_FREQUENCY;
        sleepyThread = new SleepyThread(drawInterval, this::myDraw);
        sleepyThread.start();
        Stage.start();
    }

    public void terminate() {
        if (sleepyThread == null) {
            return;
        }
        sleepyThread.terminate();
        Stage.terminate();
        Console.terminate();
    }

    private void myDraw() {
        Canvas canvas = surfaceHolder.lockCanvas();
        if (canvas == null) {
            return;
        }
        Console.draw(canvas);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }
}

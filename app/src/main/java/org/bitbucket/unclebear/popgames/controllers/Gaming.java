package org.bitbucket.unclebear.popgames.controllers;

import android.util.Log;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.games.arrow.ArrowGame;
import org.bitbucket.unclebear.popgames.games.brick.BrickGame;
import org.bitbucket.unclebear.popgames.games.racing.RacingGame;
import org.bitbucket.unclebear.popgames.games.snake.SnakeGame;
import org.bitbucket.unclebear.popgames.games.tetris.TetrisGame;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Setting;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

public class Gaming {
    private static SleepyThread sleepyThread;
    private static char chapter;
    private static Game game;

    private Gaming() {
    }

    public static void start(char chapter) {
        if (sleepyThread != null && sleepyThread.isRunning()) {
            return;
        }
        Screen.clearPixels();
        Screen.clearAuxiliaryPixels();
        final long updateInterval = Dimension.SECOND / Dimension.UPDATE_FREQUENCY;
        Gaming.chapter = chapter;
        game = getGameImplement(chapter);
        game.start();
        sleepyThread = new SleepyThread(updateInterval, Gaming::updatePixels);
        sleepyThread.start();
    }

    private static Game getGameImplement(char chapter) {
        Game result;
        switch (chapter) {
            case 'A':
                result = new ArrowGame();
                break;
            case 'B':
                result = new BrickGame();
                break;
            case 'C':
                result = new SnakeGame();
                break;
            case 'D':
                result = new RacingGame();
                break;
            case 'E':
            default:
                result = new TetrisGame();
                break;
        }
        return result;
    }

    public static void pause() {
        if (sleepyThread == null) {
            return;
        }
        game.pause();
    }

    public static void resume() {
        if (sleepyThread == null) {
            return;
        }
        game.resume();
    }

    public static void terminate() {
        if (sleepyThread == null) {
            return;
        }
        game.terminate();
        sleepyThread.terminate();
        Screen.clearPixels();
        Screen.clearAuxiliaryPixels();
        Stage.gameOver(chapter);
    }

    public static void gameOver() {
        if (sleepyThread == null) {
            return;
        }
        Sound.play(R.raw.music_game_over);
        game.terminate();
        sleepyThread.terminate();
        if (Hud.getScore() > Setting.get(Setting.SCORE + chapter, 0)) {
            Setting.set(Setting.SCORE + chapter, Hud.getScore());
        }
        Hud.setScore(0);
        Hud.setSpeed(Hud.BASE_SPEED_AND_LEVEL);
        Hud.setLevel(Hud.BASE_SPEED_AND_LEVEL);
        final int lineUpdateInterval = 25;
        for (int i = Dimension.PIXELS_AMOUNT_Y - 1; i >= 0; --i) {
            Screen.updatePixelsInRect(true, new int[]{0, i, Dimension.PIXELS_AMOUNT_X, 1});
            try {
                Thread.sleep(lineUpdateInterval);
            } catch (InterruptedException e) {
                Log.e(Dimension.ERROR, e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
        for (int i = Dimension.PIXELS_AMOUNT_Y - 1; i >= 0; --i) {
            Screen.updatePixelsInRect(false, new int[]{0, i, Dimension.PIXELS_AMOUNT_X, 1});
            try {
                Thread.sleep(lineUpdateInterval);
            } catch (InterruptedException e) {
                Log.e(Dimension.ERROR, e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
        Stage.gameOver(chapter);
    }

    public static void pressButton(Button.Key key) {
        if (game == null) {
            return;
        }
        game.pressButton(key);
    }

    public static void updatePixels() {
        if (game == null) {
            return;
        }
        game.updatePixels();
    }
}

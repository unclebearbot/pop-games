package org.bitbucket.unclebear.popgames.games.tetris.shapes;

import org.bitbucket.unclebear.popgames.games.tetris.Shape;

public class IShape extends Shape {

    public IShape(int x, int y) {
        super(x, y);
        boolean[][] block = new boolean[][]{{true}, {true}, {true}, {true}};
        setBlock(block);
    }
}

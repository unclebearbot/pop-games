package org.bitbucket.unclebear.popgames.beans;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Resource;

public class Hud {
    public static final int BASE_SPEED_AND_LEVEL = 1;
    private static MyInteger score;
    private static MyInteger speed;
    private static MyInteger level;
    private static Label highLabel;
    private static Paint pausingPaint;
    private static Label pausingLabel;
    private static boolean isShowingPausingIcon;
    private static Label scoreLabel;
    private static Label speedLabel;
    private static Label levelLabel;
    private static Bitmap pausing;
    private static Matrix pausingMatrix;

    private Hud() {
    }

    static void initialize(int x, int y, int width, int height) {
        if (score != null) {
            return;
        }
        final float numberWidthFactor = 0.07f;
        int numberWidth = (int) (width * numberWidthFactor);
        int labelHeight = numberWidth * 2;
        int tempX2 = new MyInteger(2, 0, 0, numberWidth).getWidth();
        int tempX4 = new MyInteger(4, 0, 0, numberWidth).getWidth();
        int tempX6 = new MyInteger(6, 0, 0, numberWidth).getWidth();
        int tempX8 = new MyInteger(8, 0, 0, numberWidth).getWidth();
        final float scoreY = 0.1f;
        final float speedY = 0.6f;
        final float levelY = 0.7f;
        final float pausedY = 0.9f;
        score = new MyInteger(6, x + tempX2, y + (int) (height * scoreY), numberWidth);
        score.setValue(0);
        speed = new MyInteger(2, x + tempX6, y + (int) (height * speedY), numberWidth);
        speed.setValue(BASE_SPEED_AND_LEVEL);
        level = new MyInteger(2, x + tempX6, y + (int) (height * levelY), numberWidth);
        level.setValue(BASE_SPEED_AND_LEVEL);
        final float spacingFactor = 0.01f;
        float spacing = height * spacingFactor;
        final float pausingSpacingFactor = 0.13f;
        float pausingSpacing = height * pausingSpacingFactor;
        highLabel = new Label(Resource.getString(R.string.hi), x + tempX4, y + (int) (height * scoreY - spacing), labelHeight);
        highLabel.lightUp(false);
        scoreLabel = new Label(Resource.getString(R.string.score), x + tempX8, y + (int) (height * scoreY - spacing), labelHeight);
        speedLabel = new Label(Resource.getString(R.string.speed), x + tempX8, y + (int) (height * speedY - spacing), labelHeight);
        levelLabel = new Label(Resource.getString(R.string.level), x + tempX8, y + (int) (height * levelY - spacing), labelHeight);
        pausing = Resource.getBitmap(R.drawable.paused);
        pausingPaint = new Paint();
        pausingPaint.setAntiAlias(true);
        pausingPaint.setAlpha(Dimension.LCD_ON_ALPHA);
        pausingMatrix = new Matrix();
        final float pausingWidthFactor = 0.25f;
        final float scaleLevel = width * pausingWidthFactor / pausing.getWidth();
        final float pausedOffsetX = 0.5f;
        pausingMatrix.setScale(scaleLevel, scaleLevel);
        pausingMatrix.postTranslate(x + tempX8 * pausedOffsetX, y + height * pausedY - pausingSpacing);
        pausingLabel = new Label(Resource.getString(R.string.paused), x + tempX8, y + (int) (height * pausedY), labelHeight);
        isShowingPausingIcon = true;
        showPausingIcon(false);
    }

    public static void reset() {
        score.setValue(0);
        speed.setValue(BASE_SPEED_AND_LEVEL);
        level.setValue(BASE_SPEED_AND_LEVEL);
    }

    public static int getScore() {
        if (score == null) {
            return 0;
        }
        return score.getValue();
    }

    public static void setScore(int i) {
        if (score == null) {
            return;
        }
        score.setValue(i);
    }

    public static int getSpeed() {
        if (speed == null) {
            return 0;
        }
        return speed.getValue();
    }

    public static void setSpeed(int i) {
        if (speed == null) {
            return;
        }
        speed.setValue(i);
    }

    public static int getLevel() {
        if (level == null) {
            return 0;
        }
        return level.getValue();
    }

    public static void setLevel(int i) {
        if (level == null) {
            return;
        }
        level.setValue(i);
    }

    public static void showHighLabel(boolean willShowHighLabel) {
        if (highLabel == null) {
            return;
        }
        highLabel.lightUp(willShowHighLabel);
    }

    public static void showPausingIcon(boolean willShowPausingIcon) {
        if (pausingPaint == null) {
            return;
        }
        if (willShowPausingIcon && !isShowingPausingIcon) {
            isShowingPausingIcon = true;
            pausingPaint.setAlpha(Dimension.LCD_ON_ALPHA);
            pausingLabel.lightUp(true);
        }
        if (!willShowPausingIcon && isShowingPausingIcon) {
            isShowingPausingIcon = false;
            pausingPaint.setAlpha(Dimension.LCD_OFF_ALPHA);
            pausingLabel.lightUp(false);
        }
    }

    public static void draw(Canvas canvas) {
        if (score == null) {
            return;
        }
        highLabel.draw(canvas);
        scoreLabel.draw(canvas);
        score.draw(canvas);
        speedLabel.draw(canvas);
        speed.draw(canvas);
        levelLabel.draw(canvas);
        level.draw(canvas);
        canvas.drawBitmap(pausing, pausingMatrix, pausingPaint);
        pausingLabel.draw(canvas);
    }
}

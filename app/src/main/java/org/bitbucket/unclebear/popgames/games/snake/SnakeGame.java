package org.bitbucket.unclebear.popgames.games.snake;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Button;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.beans.Screen;
import org.bitbucket.unclebear.popgames.controllers.Sound;
import org.bitbucket.unclebear.popgames.games.Game;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.Mathematics;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

public class SnakeGame extends Game {
    private Snake snake;
    private Apple apple;

    public SnakeGame() {
        setLevelUpScoreBase(50);
        setLevelUpScoreInterval(20);
        snake = new Snake(Dimension.PIXELS_AMOUNT_X / 2, Dimension.PIXELS_AMOUNT_Y / 2);
        apple = new Apple(-1, -1);
    }

    private void moveSnake() {
        snake.moveOneStep(apple);
        if (!snake.isAlive()) {
            Screen.clearPixels();
            snake = new Snake(Dimension.PIXELS_AMOUNT_X / 2, Dimension.PIXELS_AMOUNT_Y / 2);
            finallyDecreaseLife();
            return;
        }
        if (!apple.isAlive()) {
            increaseScore(10);
            Sound.play(R.raw.effect_positive);
            resetApple();
        } else {
            Sound.play(R.raw.effect_click);
        }
    }

    @Override
    public void start() {
        resetApple();
        resume();
    }

    @Override
    public void resume() {
        clearSleepyThreads();
        int baseSpeed = 2;
        addSleepyThread(new SleepyThread((int) (Dimension.SECOND / (baseSpeed * (1 + Hud.getSpeed() / 5f))), this::moveSnake));
        startAllSleepyThreads();
    }

    @Override
    public void pressButton(Button.Key key) {
        switch (key) {
            case UP:
            case DOWN:
            case LEFT:
            case RIGHT:
                snake.setOrientationByKey(key);
                break;
            case AB:
                moveSnake();
                break;
            default:
                break;
        }
    }

    @Override
    public void updatePixels() {
        Screen.updatePixels(false, snake.getTailsToClear());
        snake.clearTails();
        Screen.updatePixels(true, snake.getBody());
        Screen.updatePixelsInBlock(apple.getBlock(), apple.getX(), apple.getY());
        Screen.updateAuxiliaryPixelsInLine(getExtraLife());
    }

    private void resetApple() {
        int appleX = (int) Mathematics.getRandomBetween(0, Dimension.PIXELS_AMOUNT_X - 1L);
        int appleY = (int) Mathematics.getRandomBetween(0, Dimension.PIXELS_AMOUNT_Y - 1L);
        while (snake.isInBlockAndUp(appleX, appleY) || Screen.isOutOfBound(appleX, appleY)) {
            appleX = (int) Mathematics.getRandomBetween(0, Dimension.PIXELS_AMOUNT_X - 1L);
            appleY = (int) Mathematics.getRandomBetween(0, Dimension.PIXELS_AMOUNT_Y - 1L);
        }
        apple.setX(appleX);
        apple.setY(appleY);
        apple.setAlive(true);
    }
}

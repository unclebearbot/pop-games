package org.bitbucket.unclebear.popgames.utils;

import java.util.HashMap;

public class Setting {
    public static final String SCORE = "score";
    public static final String MUSIC = "music";
    public static final String EFFECT = "effect";
    public static final String VIBRATION = "vibration";
    public static final String SKIN = "skin";
    private static final String FILE = "settings";
    private static HashMap<String, Object> settings = new HashMap<>();

    static {
        settings = Persistence.deserialize(FILE, settings);
    }

    private Setting() {
    }

    public static void set(String key, Object value) {
        if (settings.containsKey(key)) {
            settings.remove(key);
        }
        settings.put(key, value);
        Persistence.serialize(FILE, settings);
    }

    public static <V> V get(String key, V fallback) {
        if (!settings.containsKey(key)) {
            settings.put(key, fallback);
            return fallback;
        }
        return new Caster<V>().cast(settings.get(key));
    }
}

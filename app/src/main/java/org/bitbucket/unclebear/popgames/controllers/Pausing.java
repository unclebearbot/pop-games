package org.bitbucket.unclebear.popgames.controllers;

import org.bitbucket.unclebear.popgames.R;
import org.bitbucket.unclebear.popgames.beans.Hud;
import org.bitbucket.unclebear.popgames.utils.Dimension;
import org.bitbucket.unclebear.popgames.utils.SleepyThread;

public class Pausing {
    private static boolean willShowPausingIcon;
    private static SleepyThread sleepyThread;

    private Pausing() {
    }

    public static void pause() {
        if (sleepyThread != null && sleepyThread.isRunning()) {
            return;
        }
        Sound.play(R.raw.effect_positive);
        Gaming.pause();
        final long pausingIconBlinkInterval = Dimension.MILLISECOND * 500;
        sleepyThread = new SleepyThread(pausingIconBlinkInterval, () -> {
            willShowPausingIcon = !willShowPausingIcon;
            Hud.showPausingIcon(willShowPausingIcon);
        });
        sleepyThread.start();
    }

    public static void resume() {
        Sound.play(R.raw.effect_negative);
        Gaming.resume();
        terminate();
    }

    public static void terminate() {
        if (sleepyThread == null) {
            return;
        }
        sleepyThread.terminate();
        willShowPausingIcon = false;
        Hud.showPausingIcon(false);
    }
}
